import axios from 'axios';

const api = axios.create({ 
    url: 'http://localhost:3300/api',
})

export default {
    api,
}

export const apiURL = 'http://localhost:3300/api';

export const apiURL2 = 'http://localhost:3301/api';