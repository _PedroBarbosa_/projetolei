export const distritos = [
    {
        value: 'Aveiro',
        label: 'Aveiro'
    },
    {
        value: 'Beja',
        label: 'Beja'
    },
    { value: 'Braga', label: 'Braga' },
    { value: 'Bragança', label: 'Bragança' },
    { value: 'Castelo Branco', label: 'Castelo Branco' },
    { value: 'Coimbra', label: 'Coimbra' },
    { value: 'Evora', label: 'Évora' },
    { value: 'Faro', label: 'Faro' },
    { value: 'Guarda', label: 'Guarda' },
    { value: 'Leiria', label: 'Leiria' },
    { value: 'Lisboa', label: 'Lisboa' },
    { value: 'Portalegre', label: 'Portalegre' },
    { value: 'Porto', label: 'Porto' },
    { value: 'Santarem', label: 'Santarém' },
    { value: 'Setubal', label: 'Setúbal' },
    { value: 'Viana do Castelo', label: 'Viana do Castelo' },
    { value: 'Vila_Real', label: 'Vila Real' },
    { value: 'Viseu', label: 'Viseu' }
]

export default {
    distritos,
}