import React, { useEffect, useContext, useState } from 'react';
import { Route, Redirect } from 'react-router';
import { getToken } from './services/Utils';
import { apiURL } from './AxiosConfig';
import axios from 'axios';
import context from './context'

function ProtectedRoute({ component: Component, ...rest }) {

    const token = getToken()
    const { value, setValue } = useContext(context)


    const validateToken = () => {
        axios.get(apiURL + '/verify', {
            headers: {
                authorization: 'Bearer ' + token
            }
        })
            .then((res) => {
                res.status == 200 ? setValue(res.data.id) : setValue(false)
            }).catch((error) => {
                console.error(error)
            });
    }


    useEffect(() => {
        validateToken()
    }, [])

    return (
        <Route {...rest} render={(props) => value ? (<Component {...props} />) : (<Redirect to='/' />)
        } />
    );
}

export default ProtectedRoute;