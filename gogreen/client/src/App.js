import React, { useEffect, useState } from 'react';
import HeaderLoggedOut from './components/Header/Header';
import Main from './components/Main/Main';
import bac from './images/12451.jpg'
// eslint-disable-next-line no-unused-vars
import css from './App.css'
import LoginContext from './context'
import { getToken } from './services/Utils'
import axios from 'axios';
import { apiURL } from './AxiosConfig';
import FuncContext from './FuncContext';


function App() {

  const token = getToken();
  const [value, setValue] = useState();
  const [isFunc, setIsFunc] = useState(false);

  const validateToken = () => {
    axios.get(apiURL + '/verify', {
      headers: {
        authorization: 'Bearer ' + token
      }
    })
      .then((res) => {
        if (res.status === 200) setValue(res.data.id)
        else setValue(false)
      }).catch((error) => {
      });
  }

  useEffect(() => {
    if (token) validateToken()
  }, [])

  return (
    <div style={appStyle}>
      <LoginContext.Provider value={{ value, setValue }}>
        <FuncContext.Provider value={{ isFunc, setIsFunc }}>
          <HeaderLoggedOut />
          <Main />
        </FuncContext.Provider>
      </LoginContext.Provider>
    </div >
  );
}


const appStyle = {
  backgroundImage: 'url(' + bac + ')',
  backgroundSize: 'cover',
  minHeight: '100vh',
  minWidth: '100%',
  backgroundRepeat: 'repeat-y'
}

export default App;
