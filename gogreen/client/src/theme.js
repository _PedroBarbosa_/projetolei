import {createMuiTheme} from '@material-ui/core/styles';
import {colors} from '@material-ui/core';

export const theme = createMuiTheme({
    palette: {
        primary: {
            dark: colors.green[800],
            main: colors.green[400],
            light: colors.green[100],
            contrastText: colors.green[500]
        },
        secondary:{
            dark: colors.grey[800],
            main: colors.grey[500],
            light: colors.grey[100],
            contrastText: colors.grey[500]
        },
    },
    status: {
        error: 'red',
        danger: 'orange'
    }
})