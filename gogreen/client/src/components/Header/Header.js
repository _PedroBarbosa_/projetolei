import React, { useContext, useEffect } from 'react';
import { Button } from '@material-ui/core'
import { theme } from '../../theme';
import { ThemeProvider } from '@material-ui/core/styles';
import { makeStyles } from '@material-ui/core/styles';
import LoginField from './LoginField';
import { Link } from 'react-router-dom';
import UserLoggedInField from './UserLoggedInField';
import ContextLogin from '../../context'
import logo from '../../images/gogreenLogoLetterbox.png'
import FuncContext from '../../FuncContext';


//Overwrite dos estilos do material UI
const useStyles = makeStyles({
    buttonStyle: {
        marginRight: '4vw',
        marginTop: '5px',
        paddingBottom: 0,
        color: 'forestgreen',
        alignSelf: 'flex-end'
    },
    headerStyle: {
        //marginBottom: '4vh',
        paddingBottom: '1vh',
        //background: 'linear-gradient(90deg, rgba(86,171,47,1) 0%, rgba(168,224,99,1) 100%)',
        display: 'flex',

    },
    subDivs: {
        marginRight: 20,
        margin: 'auto',
        float: 'right',
        padding: 0,
        display: 'flex'
    },
    linkStyle: {
        textDecoration: 'none',
        '&:visited, &:link, &:active': {
            textDecoration: 'none',
            color: 'inherit',
        }
    },
    logo: {
        height: '7vh',
        marginTop: '1vh',
        marginLeft: '1vw'

    }
})


function HeaderLoggedOut() {

    /**Hooks  */
    //Hook dos estildos do material UI
    const classes = useStyles();
    //Hook context para a verificação se o utilizador está autenticado
    const { value } = useContext(ContextLogin);
    const { isFunc, setIsFunc } = useContext(FuncContext);

    return (
        <div>
            <div className={classes.headerStyle}>
                {value ? <Link to="/"><img src={logo} alt="logo" className={classes.logo} /></Link> : null}
                <div className={classes.subDivs}>
                    <ThemeProvider theme={theme}>
                        {value ? isFunc ? null : <Button className={classes.buttonStyle}><Link to="/UserDashboard" className={classes.linkStyle}>Painel</Link></Button> : <Button className={classes.buttonStyle}><Link to="/" className={classes.linkStyle}>Home</Link></Button>}
                        {value ? isFunc ? null : <Button className={classes.buttonStyle}><Link to="/Campanhas" className={classes.linkStyle}>Campanhas</Link></Button> : null}
                        {value ? isFunc ? null : <Button className={classes.buttonStyle}><Link to="/Market" className={classes.linkStyle}>Market</Link></Button> : null}
                        {isFunc ? <Button className={classes.buttonStyle}><Link to="/Funcionarioboard" className={classes.linkStyle}>Recolhas</Link></Button> : null}
                        <Button className={classes.buttonStyle}>
                            <Link to="/Descobrir" className={classes.linkStyle}>Reciclar</Link>
                        </Button>
                        <div className={classes.subDivs}>
                            {value ? <UserLoggedInField /> : <LoginField />}
                        </div>
                    </ThemeProvider>
                </div>
            </div>
        </div>
    );
}

export default HeaderLoggedOut;