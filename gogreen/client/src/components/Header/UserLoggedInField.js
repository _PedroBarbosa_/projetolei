import React, { useContext, useEffect, useState } from 'react';
import { Button, makeStyles, MenuItem, Menu, Typography, ThemeProvider, Badge, Drawer } from '@material-ui/core';
import { logout } from '../../services/Utils'
import ContextLogin from '../../context'
import AccountBoxIcon from '@material-ui/icons/AccountBox';
import { theme } from '../../theme';
import { Redirect, useHistory } from 'react-router-dom';
import axios from 'axios';
import { apiURL } from '../../AxiosConfig';
import NotificationsDrawer from '../NotificationsDrawer';
import FuncContext from '../../FuncContext'

const useStyles = makeStyles((theme) => ({
    buttonStyle: {
        marginRight: '4vw',
        marginTop: '5px',
        paddingBottom: 0,
        alignSelf: 'flex-end',
        color: theme.palette.text.secondary
    },
    nameStyle: {
        display: 'inline-block',
        marginRight: '2vw',
        marginTop: '3vh',
        backgroundColor: 'white',
        marginBottom: '2vh',
        padding: '5px 10px 5px 10px',
        borderRadius: 10,
        boxShadow: '2px 2px 10px grey',
        fontFamily: 'Roboto',
        fontWeight: 'bold',
    },
    AccountBoxIcon: {
        height: 'inherit',

    },
    menuEntry: {
        color: theme.palette.text.secondary
    },
    badgeStyle: {
        display: 'flex',
        flexDirection: 'column',
        '& > *': {
            marginBottom: theme.spacing(2),
        },
        '& .MuiBadge-root': {
            marginRight: theme.spacing(4),
        },

    },
    topDiv: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'flex-end'
    },
    paperAnchorRight: {
        backgroundColor: '#0B0C10',
        width: '20vw',

    },

}));

function UserLoggedInField(props) {

    const classes = useStyles();
    const { value, setValue } = useContext(ContextLogin);
    const { isFunc, setIsFunc } = useContext(FuncContext)
    const [invisible, setInvisible] = useState(true);
    const [openNotification, setOpenNotification] = useState();
    const hist = useHistory();
    let tempUser2;
    let tempFunc2;



    if (isFunc) {
        if (localStorage.getItem('func')) {
            const tempFunc = localStorage.getItem('func');
            tempFunc2 = JSON.parse(tempFunc)
        }
    } else {
        if (localStorage.getItem('utilizador')) {
            const tempUser = localStorage.getItem('utilizador');
            tempUser2 = JSON.parse(tempUser)
        }
    }

    useEffect(() => {
        if (!isFunc) {
            const fetchNot = async () => {
                await axios.get(apiURL + '/utilizadorNotificacoes/' + tempUser2.id_utilizador)
                    .then((res) => {
                        if (res.data.length > 0) {
                            setInvisible(false)
                        }
                        else {
                            setInvisible(true)
                        }
                    })
                    .catch((error) => {

                    })
            }
            fetchNot()
        }
    }, [])

    const openConta = () => hist.push("/editPerfil")
    const [anchorEl, setAnchorEl] = React.useState(null);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    return (
        <div className={classes.topDiv}>
            <Button aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick} className={classes.buttonStyle}>
                <Badge color="error" variant="dot" anchorOrigin={{ vertical: 'top', horizontal: 'right' }} invisible={invisible}>
                    <AccountBoxIcon style={{ marginRight: '10px' }} />{isFunc ? (tempFunc2.username > 10 ? tempFunc2.username.substring(0, 12) + '...' : tempFunc2.username) : (tempUser2.nome.length > 10 ? tempUser2.nome.substring(0, 12) + '...' : tempUser2.nome)}
                </Badge>
            </Button>
            <ThemeProvider theme={theme}>
                <Typography color="secondary">

                    <Menu
                        id="simple-menu"
                        anchorEl={anchorEl}
                        keepMounted
                        open={Boolean(anchorEl)}
                        onClose={handleClose}
                    >
                        {isFunc ? null : <MenuItem className={classes.menuEntry} onClick={() => setOpenNotification(true)}>Notificações</MenuItem>}
                        {isFunc ? null : <MenuItem className={classes.menuEntry} onClick={() => openConta()}>Conta</MenuItem>}
                        <MenuItem className={classes.menuEntry} onClick={() => {
                            setValue(false);
                            setIsFunc(false);
                            logout();
                            return <Redirect to="/" />
                        }}>Sair</MenuItem>
                    </Menu>
                </Typography>
            </ThemeProvider>
            <Drawer anchor={'right'} open={openNotification} elevation={10} onClose={() => setOpenNotification(false)} variant="temporary" classes={{ paperAnchorRight: classes.paperAnchorRight }}>
                <NotificationsDrawer />
            </Drawer>
        </div >
    );
}

export default UserLoggedInField;