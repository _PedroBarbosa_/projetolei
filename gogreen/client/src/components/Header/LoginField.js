import React, { useState, useContext } from 'react'
import { makeStyles } from '@material-ui/core/styles';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import { Field, Form, Formik } from 'formik';
import { TextField } from 'formik-material-ui'
import { Button, Snackbar } from '@material-ui/core';
import * as Yup from 'yup';
import axios from 'axios';
import { useHistory } from 'react-router-dom';
import ContextLoggedIn from '../../context'
import { apiURL } from '../../AxiosConfig'
import FuncContext from '../../FuncContext';

// overwrite dos estilos do material UI
const useStyles = makeStyles({
    inputStyle: {
        margin: 0,
        marginTop: 5,
        padding: 0,
        color: 'black',
        marginRight: '3vh',
        '.Mui-error': {
            color: 'gray'
        },
        '.MuiFormHelperText-root': {
            color: 'black'
        },
        '.Mui-focused': {
            color: 'purple'
        }
    },
    iconStyle: {
        marginTop: 10,
        height: '2vh'
    },
    divStyle: {
        alignContent: 'center',
        alignItems: 'center',
        height: 'inherit',
        verticalAlign: 'middle'
    }
})

// Valores iniciais para o formulario de login
const vals = {
    email: '',
    password: '',
}

// esquema de validação dos campos de login
const validSchema =
    Yup.object().shape({
        password: Yup.string()
            .required('Por favor introduza a palavra chave'),
        email: Yup.string()
            .email('Email inválido')
            .required('Por favor introduza o email'),
    })



export default function LoginField() {

    /** Zona das funçoes deste componente (LoginField) */
    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        setOpen(false);
    };

    const handleSubmit = values => {
        axios.post(apiURL + '/login', {
            'email': values.email,
            'password': values.password,
        })
            .then(function (response) {
                if (response.data.utilizador && response.data.utilizador.hasOwnProperty("id_utilizador")) {
                    localStorage.setItem('token', JSON.stringify(response.data.token))
                    localStorage.setItem('utilizador', JSON.stringify(response.data.utilizador))
                    setValue(response.data.id_utilizador)
                    history.push('/UserDashboard');
                } 
                if (response.data.funcionario && response.data.funcionario.hasOwnProperty("id_funcionario")) {
                    localStorage.setItem('func', JSON.stringify(response.data.funcionario))
                    localStorage.setItem('token', JSON.stringify(response.data.token))
                    setIsFunc(true)
                    setValue(response.data.funcionario.id_funcionario)
                    setTimeout(() => history.push('/Funcionarioboard'),200)
                }
            }).catch(function (error) {
                console.log(error)
                setOpen(true);
            })
    }


    /** Hooks deste compoenente */
    const { value, setValue } = useContext(ContextLoggedIn);
    const {isFunc , setIsFunc} = useContext(FuncContext)
    // hook para o snaackbar do login
    const [open, setOpen] = useState(false);
    // hook para sobrepor os estilos do material UI
    const classes = useStyles();
    // hook do browserRouter
    let history = useHistory();

    /** UI */
    return (
        <div className={classes.divStyle}>
            {value}
            <Formik
                initialValues={vals}
                onSubmit={handleSubmit}
                validationSchema={validSchema}
            >
                {({ dirty, isValid, values }) => {
                    return (
                        <Form className={classes.divStyle}>
                            <Field
                                key="email"
                                name="email"
                                type="text"
                                label="email"
                                component={TextField}
                                required
                                disabled={false}
                                className={classes.inputStyle}

                            />
                            <Field
                                key="password"
                                name="password"
                                type="password"
                                label="password"
                                component={TextField}
                                required
                                disabled={false}
                                className={classes.inputStyle}

                            />
                            <Button disabled={!isValid || !dirty || values.password.length < 6} type="Submit" style={{ marginTop: 20 }} ><ArrowForwardIosIcon /></Button>
                        </Form>
                    )
                }}
            </Formik>
            <Snackbar open={open} autoHideDuration={2000} onClose={handleClose} message="Autenticação inválida!" anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}>
            </Snackbar>
        </div>
    )
}
