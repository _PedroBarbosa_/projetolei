import React from 'react'
import { Grid, makeStyles, Paper, ThemeProvider, Typography } from '@material-ui/core';
import logo from '../../../images/gogreenLogoSecondary.png';
import { theme } from '../../../theme';
import RegMatUI from './RegMatUI';


const useStyles = makeStyles((theme) => ({
    gridMain: {
        // padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
        height: 'inherit',
        //padding: '2vh',
        marginTop: '10vh',
        width: 'inherit'
    },
    paper: {
        width: '50vh',
        padding: '3vh',
        paddingTop: '0.1vh'
    },
    logoStyle: {
        height: '5vw',
        marginBottom: '2vh',
        alignItems: 'center',
        alignContent: 'center'
    },
    inputsGrid: {
        marginTop: '3vh',
        padding: '4vh',
        width: 'inherit',
    },
    upperGrid: {
        width: '300vh',
        margin: '0 auto'
    },
    input: {
        width: 'inherit'
    }
}));

export default function Registar() {

    const classes = useStyles();

    return (
        <div style={styleHome}>
            <Grid item xl={6}>
                <Grid container direction="row" justify="center" spacing={2} className={classes.gridMain}>
                    <Grid item xs={6} sm={5} style={{marginRight: '4vw', textAlign: 'left' }}>
                        <Typography display="inline" variant="h3" align={'left'} style={{backgroundColor: 'rgb(245, 245, 245, 0.3)',lineHeight: 1.3}}>
                            Junte-se a milhares de pessoas
                            a ajudar a tornar o mundo, 
                            num local mais limpo.
                        </Typography>
                    </Grid>
                    <Grid item >
                        <Paper elevation={10} className={classes.paper}>
                            <Grid container direction="column" justify="center" spacing={2} className={classes.gridMain}>
                                {/** Grid para o cabeçalho do registo */}
                                <Grid item xs={6} sm={3} className={classes.upperGrid}>
                                    <img src={logo} alt="logo" className={classes.logoStyle} />
                                    <ThemeProvider theme={theme}>
                                        <Typography color="primary" display="block" variant="h3" style={{ marginLeft: '-50%' }}>
                                            GoGreen
                                        </Typography>
                                    </ThemeProvider>
                                </Grid>
                                {/** Grid dos inputs para o registo */}
                                <Grid item xs={6} sm={3} className={classes.inputsGrid} >
                                    <RegMatUI />
                                </Grid>
                            </Grid>
                        </Paper>
                    </Grid>
                </Grid>
            </Grid>
        </div>
    )
}


const styleHome = {
    //backgroundImage: 'url(' + background + ')',
    //backgroundColor: '#FFFFFF',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: '50% 50%',
    backgroundSize: 'auto',
    marginRight: '5vw',
    marginLeft: '5vw',
}