import React from 'react';
import { Form, Field, Formik } from 'formik';
import { TextField } from 'formik-material-ui'
import * as Yup from 'yup';
import { Button, MenuItem, Snackbar } from '@material-ui/core';
import { distritos } from '../../../distritos';
import { makeStyles, ThemeProvider } from '@material-ui/core/styles';
import axios from 'axios';
import { theme } from '../../../theme';

const useStyles = makeStyles({
    formStyle: {
        width: 'inherit',
        alignContent: 'center'
    },
    inputStyle: {
        width: '80%',
        '& label.Mui-focused': {
            color: 'green',
        },
        '& .MuiInput-underline:after': {
            borderBottomColor: 'green',
        },
    },
    buttonStyle: {
        marginTop: '5vh',
        display: 'block',
        alignContent: 'center',
        margin: '0 auto',
        color: 'white'
    }
});

const vals = {
    email: '',
    nome: '',
    password: '',
    morada: '',
    distrito: '',
    dataInscricao: new Date(),
    perfil_ativo_recolha: 0
}
const validSchema =
    Yup.object().shape({
        morada: Yup.string()
            .min(3, 'email demasiado curto!')
            .required('Obrigatório'),
        password: Yup.string()
            .min(6, 'Palavra chave deverá ter entre 6-20 caracteres!')
            .max(50, 'Palavra chave deverá ter entre 6-20 caracteres!')
            .required('Obrigatório'),
        email: Yup.string()
            .email('Email inválido')
            .required('Obrigatório'),
        distrito: Yup.string()
            .required(),
    })

export default function RegMatUI() {

    const [open, setOpen] = React.useState(false);

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        setOpen(false);
    };

    const classes = useStyles();

    const handleSubmit = values => {
        axios.post('http://localhost:3300/api/utilizadores', {
            nome: values.nome,
            email: values.email,
            password: values.password,
            morada: values.morada,
            distrito: values.distrito,
            dataInscricao: values.dataInscricao,
            perfil_ativo_recolha: values.perfil_ativo_recolha
        })
            .then(function (response) {
                if (response.status > 199 && response.status < 299) setOpen(true);
                console.log(response)
            }).catch(function (error) {
                console.log(error)
            })
            .then(function () {

            });
    }


    return (
        <div style={{ width: 'inherit' }}>
            <Formik
                initialValues={vals}
                onSubmit={handleSubmit}
                validationSchema={validSchema}
            >
                {({ dirty, isSubmitting, isValid }) => {
                    return (
                        <Form className={classes.formStyle}>
                            <Field
                                key="email"
                                name="email"
                                type="input"
                                label="email"
                                component={TextField}
                                required
                                className={classes.inputStyle}
                            />
                            <Field
                                key="nome"
                                name="nome"
                                type="input"
                                label="nome"
                                component={TextField}
                                required
                                className={classes.inputStyle}
                            />
                            <Field
                                key="password"
                                name="password"
                                type="password"
                                label="password"
                                component={TextField}
                                required
                                className={classes.inputStyle}
                            />

                            <Field
                                key="morada"
                                name="morada"
                                type="input"
                                label="morada"
                                component={TextField}
                                className={classes.inputStyle}
                            />

                            <Field
                                key="distrito"
                                name="distrito"
                                type="text"
                                label="distrito"
                                select
                                placeholder="Escolha distrito"
                                helperText="Por favor escolha o distrito"
                                required
                                component={TextField}
                                className={classes.inputStyle}>
                                {distritos.map(distrito => (<MenuItem key={distrito.value} value={distrito.value}>{distrito.label}</MenuItem>))}
                            </Field>
                            <ThemeProvider theme={theme}>
                                <Button variant="contained" color="primary" disabled={!dirty || !isValid || isSubmitting} className={classes.buttonStyle} type="submit">Registar</Button>
                            </ThemeProvider>
                        </Form>
                    )
                }}
            </Formik >
            <Snackbar open={open} autoHideDuration={2000} onClose={handleClose} message="Registo com sucesso!" anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}>
            </Snackbar>
        </div>
    );
};

