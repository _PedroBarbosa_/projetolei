import React from 'react';
import { makeStyles, Paper } from '@material-ui/core';
import HowToVoteIcon from '@material-ui/icons/HowToVote';
import EqualizerIcon from '@material-ui/icons/Equalizer';
import LayersIcon from '@material-ui/icons/Layers';
import CloseIcon from '@material-ui/icons/Close';

const useStyles = makeStyles((theme) => ({
    paper: {
        padding: theme.spacing(2),
        color: theme.palette.text.secondary,
        flexGrow: 1,
        height: 'inherit',
        boxShadow: '3px 3px 10px',
        width: '50vw',
    },
    itemDiv: {
        marginTop: '3vh',
        padding: '5vh',
        textAlign: 'left',
    },
    iconDiv: {
        display: 'inline',
        marginRight: '5vw',
        float: 'left'
    },
    entryDiv: {
        border: '1px solid gray',
        margin: '2vh',
        borderRadius: 10,
        padding: 20,
        verticalAlign: 'middle',
        paddingLeft: '3vw'
    },
    contentDiv: {
        marginRight: '30px',
        verticalAlign: 'middle'
    },
    iconsStyle: {
        marginRight: '7px',
        verticalAlign: 'middle',
        maxHeight: '20px'
    }

}));

function MyRecolhasItem(props) {

    const classes = useStyles();

    return (
        <div className={classes.itemDiv}>
            <Paper className={classes.paper}>
                <CloseIcon fontSize="large" onClick={props.handleClose} style={{ float: 'right' }} />
                <div style={{marginTop: '46px'}}>
                    {props.values.map((row, index) => (
                        <div className={classes.entryDiv} key={index}>
                            <span className={classes.contentDiv}>{index}</span>
                            <HowToVoteIcon className={classes.iconsStyle} />
                            <span className={classes.contentDiv}>{row.Nome_residuo}</span>
                            <EqualizerIcon className={classes.iconsStyle} />
                            <span className={classes.contentDiv}>{row.quantidade}</span>
                            <LayersIcon className={classes.iconsStyle} />
                            <span className={classes.contentDiv}>{row.cat}</span>
                        </div>
                    ))}
                </div>
            </Paper>
        </div>
    );
}

export default MyRecolhasItem;