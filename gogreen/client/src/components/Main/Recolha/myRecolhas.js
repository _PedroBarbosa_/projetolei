import React, { useEffect, useState, useContext } from 'react';
import { makeStyles, Paper, TableContainer, Table, TableHead, TableRow, TableCell, TableBody, Button, Modal, Grid, Typography, ThemeProvider } from '@material-ui/core';
import axios from 'axios'
import { apiURL } from '../../../AxiosConfig';
import { getCurrentUser } from '../../../services/Utils';
import Loading from '../Loading';
import MyRecolhasItem from './myRecolhasItem';
import DoneIcon from '@material-ui/icons/Done';
import HistoryIcon from '@material-ui/icons/History';
import ContextLogin from '../../../context'
import IdError from '../IdError';
import { theme } from '../../../theme'

const useStyles = makeStyles((theme) => ({
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
    topDiv: {
        marginTop: '3vh',
        width: '80vw',
        alignItems: 'center'
    },
    linkStyle: {
        textDecoration: 'none',
        '&:visited, &:link': {
            textDecoration: 'none',
            color: 'inherit',
        },
        '&:active': {
            backgroundColor: 'grey'
        },
        '& :hover': {
            backgroundColor: 'lightgrey'
        }
    },
    norec: {
        marginTop: 10,
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
        width: '80vw'
    }
}));



function MyRecolhas(props) {

    const classes = useStyles()
    const [recolhas, setRecolhas] = useState()
    const [loading, setLoading] = useState(true)
    const [open, setOpen] = useState(false);
    const [rec, setRec] = useState();

    const handleOpen = (values) => {
        setRec(values)
        setOpen(true);
    }

    const handleClose = () => {
        setOpen(false);
    }

    const { value } = useContext(ContextLogin);
    const body = <MyRecolhasItem values={rec} handleClose={handleClose} />

    var id;
    if (getCurrentUser()) id = getCurrentUser().id_utilizador;

    useEffect(() => {
        axios.get(apiURL + '/recolhasU/' + id)
            .then((res) => {
                const temp = res.data.reduce((r, a) => {
                    r[a.id_recolha] = r[a.id_recolha] || []
                    r[a.id_recolha].push(a);
                    return r;
                }, Object.create(null));
                setRecolhas(temp);
            })
            .catch((error) => {
                setRecolhas(null)
                console.log(error)
            })
            .then(() => {
            }).then(() => {
                setLoading(false)
            })
    }, [id])

    if (!value) {
        return (
            <IdError />
        )
    }

    return (
        loading ? <Loading /> :
            <div style={divStyle}>
                <Grid container direction="column" justify="center" alignItems="center">
                    <Grid item xs={12} className={classes.topDiv}>
                        <ThemeProvider theme={theme}>
                            <Paper className={classes.paper}>
                                <Typography variant="h5" color="secondary">
                                    As minhas Recolhas
                            </Typography>
                            </Paper>
                        </ThemeProvider>
                    </Grid>
                    {!recolhas ? <Paper fullwidth className={classes.norec}>Sem recolhas marcadas! </Paper> :
                        <TableContainer component={Paper} className={classes.topDiv} elevation={2}>
                            <Table className={classes.table} aria-label="simple table">
                                <TableHead>
                                    <TableRow>
                                        <TableCell>Realizada</TableCell>

                                        <TableCell align="left">Data de recolha</TableCell>
                                        <TableCell align="left">Ordem</TableCell>
                                        <TableCell align="left"></TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {Object.values(recolhas).map((row, index) => (
                                        <TableRow key={index}>
                                            <TableCell component="th" scope="row">
                                                {row[0].realizada ? <DoneIcon /> : <HistoryIcon />}
                                            </TableCell>

                                            <TableCell align="left">{new Date(row[0].data).toLocaleDateString()}</TableCell>
                                            <TableCell açign="left">{index + 1}</TableCell>
                                            <TableCell align="right"><Button onClick={() => handleOpen(row)}>Ver</Button></TableCell>
                                        </TableRow>
                                    ))}
                                </TableBody>
                            </Table>
                        </TableContainer>}
                </Grid >
                <Modal
                    open={open}
                    onClose={handleClose}
                    aria-labelledby="simple-modal-title"
                    aria-describedby="simple-modal-description"
                >
                    {body}
                </Modal>
            </div >
    );
}

const divStyle = {
    display: 'flex',
    justifyContent: 'center'
}

export default MyRecolhas;

