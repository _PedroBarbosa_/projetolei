import React, { useEffect, useState } from 'react';
import { Paper, makeStyles, Grow } from '@material-ui/core';
import { theme } from '../../../theme';
import axios from 'axios';
import { getCurrentUser } from '../../../services/Utils'
import { apiURL } from '../../../AxiosConfig';
import Loading from '../Loading'

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing(3),
        color: theme.palette.text.secondary,
    },
    buttonNav: {
        paddingRight: 20,
        borderRadius: 0

    }
}));

function RecolhasMarcadas(props) {

    const classes = useStyles();
    const id = getCurrentUser().id_utilizador
    const [recolhas, setRecolhas] = useState()
    const [loading, setLoading] = useState(true)

    useEffect(() => {
        setTimeout(() => {
            axios.get(apiURL + '/recolhasU/' + id)
                .then((res) => {
                    setRecolhas(res.data)
                    setLoading(false)
                }).catch((error) => {
                    console.log(error)
                })
        }, 2000)
    }, [recolhas])

    return (
        loading ? <Loading />  :
        <div>
            <Grow in timeout={2000}>
                <Paper className={classes.paper} elevation={2} >
                    <b>Recolhas</b>
                    <p>{JSON.stringify(recolhas,null,2)}</p>
                </Paper>
            </Grow >
        </div >
    );
}

export default RecolhasMarcadas;