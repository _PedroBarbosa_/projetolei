import React, { useState, useEffect, useRef } from 'react';
import io from 'socket.io-client'
import { makeStyles, Button, TextField, Input } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
    chatWindow: {
        width: '200px',
        position: 'absolute',
        bottom: 0,
        right: '5%'
    },
    headerChat: {
        backgroundColor: 'lightGray',
        color: 'black',

    },
    mainChat: {
        backgroundColor: 'whitesmoke',
        color: 'black',

    }

}));

function ChatWindow(props) {

    const classes = useStyles();
    const [id, setID] = useState()
    const [messages, setMessages] = useState([])
    const [message, setMessage] = useState("")
    const socketRef = useRef()
    const inRef = useRef()

    useEffect(() => {
        socketRef.current = io.connect("http://localhost:3301/")
        socketRef.current.connect("setID", id => {
            setID(id)
        })

        socketRef.current.on("sendMessage", message => {
            recieveMessage(message);
        })
    }, [])

    const recieveMessage = (message) => {
        setMessages(prevMessage => [...prevMessage, message])
    }

    const sendMessage = (e) => {
        e.preventDefault();

        const m = {
            body: m,
            id: id,
        };
        setMessage("");
        socketRef.current.emit("sendMessage", m);
    }

    const handleChange = (e) => {
        setMessage(e.target.value);
    }


    return (
        <div className={classes.chatWindow}>
            <div className={classes.headerChat}>

            </div>
            <div className={classes.mainChat}>

            </div>
            <div className={classes.inputChat}>e
                <Input fullWidth inputRef={inRef}></Input>
                <Button variant="contained" >
                    
                </Button>
            </div>
        </div>
    );
}

export default ChatWindow;