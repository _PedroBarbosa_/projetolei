import React from 'react';
import { Typography } from '@material-ui/core';

function NotificationItem(props) {
    return (
        <Typography variant={'body1'} >
            <div style={{ border: '1px solid whitesmoke', margin: '0px 10px 5px 10px', borderRadius: 3, color: 'white', padding: 10, overflow: 'auto', wordWrap: 'break-word' }}>
                <p style={{ marginBottom: '2vh', color: 'white' }}>{props.values.titulo}</p>
                <p style={{ color: 'rgba(245,245,245,0.3)' }}>{props.values.descricao}</p>
            </div>
        </Typography>
    );
}

export default NotificationItem;