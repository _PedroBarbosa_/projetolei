import React from 'react'
import { Grid, makeStyles, Paper, ThemeProvider, Typography } from '@material-ui/core';
import { theme } from '../../theme';
import userIcon from '../../images/userIcon.png'
import EditProfileForm from './EditProfileForm';


const useStyles = makeStyles((theme) => ({
    gridMain: {
        // padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
        height: 'inherit',
        //padding: '2vh',
        marginTop: '5vh',
        width: 'inherit'
    },
    paper: {
        width: '50vh',
        padding: '3vh',
        paddingTop: '0.1vh'
    },
    logoStyle: {
        height: '5vw',
        marginBottom: '2vh',
        alignItems: 'center',
        alignContent: 'center',
        border: '1px solid gray',
        borderRadius: '100%',

    },
    inputsGrid: {
        marginTop: '3vh',
        padding: '4vh',
        width: 'inherit',
    },
    upperGrid: {
        width: '300vh',
        margin: '0 auto',
        textAlign: 'center'
    },
    input: {
        width: 'inherit'
    }
}));

export default function EditProfile() {

    const classes = useStyles();

    return (
        <div style={styleHome}>
            <Grid item xl={6}>
                <Grid container direction="row" justify="center" spacing={2} className={classes.gridMain}>
                    <Grid item >
                        <Paper elevation={10} className={classes.paper}>
                            <Grid container direction="column" justify="center" spacing={2} className={classes.gridMain}>
                                {/** Grid para o cabeçalho do registo */}
                                <Grid item xs={6} className={classes.upperGrid}>
                                    <img src={userIcon} className={classes.logoStyle} alt="user icon" />
                                    <ThemeProvider theme={theme}>
                                        <Typography color="secondary" display="block" variant="h6" align="center">
                                            Editar Perfil
                                        </Typography>
                                    </ThemeProvider>
                                </Grid>
                                {/** Grid dos inputs para o registo */}
                                <Grid item xs={6} sm={3} className={classes.inputsGrid} >
                                    <EditProfileForm />
                                </Grid>
                            </Grid>
                        </Paper>
                    </Grid>
                </Grid>
            </Grid>
        </div>
    )
}


const styleHome = {
    //backgroundImage: 'url(' + background + ')',
    //backgroundColor: '#FFFFFF',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: '50% 50%',
    backgroundSize: 'auto',
    marginRight: '5vw',
    marginLeft: '5vw',
}