import React, { useEffect, useContext, useState } from 'react';
import { Form, Field, Formik } from 'formik';
import { TextField } from 'formik-material-ui'
import * as Yup from 'yup';
import { Button, MenuItem, Snackbar, Typography } from '@material-ui/core';
import { distritos } from '../../distritos';
import { makeStyles, ThemeProvider } from '@material-ui/core/styles';
import axios from 'axios';
import { theme } from '../../theme';
import LoginContext from '../../context'
import { apiURL } from '../../AxiosConfig'
import { getToken } from '../../services/Utils'
import Loading from './Loading'

const useStyles = makeStyles({
    formStyle: {
        width: 'inherit',
        alignContent: 'center'
    },
    inputStyle: {
        width: '80%',
        '& label.Mui-focused': {
            color: 'green',
        },
        '& .MuiInput-underline:after': {
            borderBottomColor: 'green',
        },
        marginBottom: 15,

    },
    buttonStyle: {
        marginTop: '5vh',
        display: 'block',
        alignContent: 'center',
        margin: '0 auto',
        color: 'white'
    },
    labelStyle: {
        display: 'block',
        marginLeft: '30px'
    },
    textStyle: {
        color: 'lightgray',
        display: 'block',
        fontWeight: 'bold',
        marginBottom: 15,
    }

});

const validSchema =
    Yup.object().shape({
        morada: Yup.string()
            .min(3, 'morada demasiado curto!'),
        password: Yup.string()
            .min(6, 'Palavra chave deverá ter entre 6-20 caracteres!')
            .max(50, 'Palavra chave deverá ter entre 6-20 caracteres!'),
        email: Yup.string()
            .email('Email inválido'),
        password2: Yup.string()
            .oneOf([Yup.ref('password'), null], 'Passwords tem de ser iguais!'),
        nome: Yup.string()
            .min(3, 'Nome demasiado curto!')
            .max(25, 'Nome apenas pode conter no máximo 25 caracteres!'),
    })

export default function EditProfileForm() {

    const [open, setOpen] = React.useState(false);
    const { value } = useContext(LoginContext)
    const [loading, setLoading] = useState(true)
    const [user, setUser] = useState()
    const token = getToken();

    const [editEmail, setEditEmail] = useState()
    const [editNome, setEditNome] = useState()
    const [editMorada, setEditMorada] = useState()
    const [editDistrito, setEditDistrito] = useState()
    const [editPassword, setEditPassword] = useState()
    const [editPassword2, setEditPassword2] = useState()

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        setOpen(false);
    };

    const classes = useStyles();

    const getUser = async () => {
        await axios.get(apiURL + '/utilizadores/' + value, {
            headers: {
                authorization: 'Bearer ' + token
            }
        })
            .then((res) => {
                setUser(res.data)
                setLoading(false)
            }).catch((error) => {
                console.log(error)
            })
    }

    useEffect(() => {
        getUser()
    }, [])

    const handleSubmit = values => {
        if (editPassword2) {
            axios.put(apiURL + '/utilizadores/'+ value, {
                id_utilizador: value,
                nome: values.nome,
                email: values.email,
                password: values.password,
                morada: values.morada,
                distrito: values.distrito,
            })
                .then(function (response) {
                    if (response.status == 200)
                        setOpen(true);
                }).catch(function (error) {
                    console.log(error)
                })
        } else {
            axios.put(apiURL + '/utilizadores/'+ value, {
                id_utilizador: value,
                nome: values.nome,
                email: values.email,
                morada: values.morada,
                distrito: values.distrito,
            })
                .then(function (response) {
                    if (response.status == 200)
                        setOpen(true);
                }).catch(function (error) {
                    console.log(error)
                })
        }
    }

    return (
        loading ? <Loading /> :
            <div style={{ width: 'inherit' }}>
                <Formik
                    initialValues={{
                        email: user.email,
                        nome: user.nome,
                        password: '*******',
                        password2: '',
                        morada: user.morada,
                        distrito: user.distrito,
                    }}
                    onSubmit={handleSubmit}
                    validationSchema={validSchema}
                >
                    {({ dirty, isSubmitting, isValid, values }) => {
                        return (
                            <Form className={classes.formStyle}>
                                <ThemeProvider theme={theme}>
                                    <Typography variant={'body1'} component={'span'} className={classes.labelStyle} align="left">
                                        <b style={{ display: 'block' }}>Email: </b>
                                        {!editEmail ? <span onClick={() => setEditEmail(true)} className={classes.textStyle}>{values.email}</span> :
                                            <Field
                                                key="email"
                                                name="email"
                                                type="input"
                                                component={TextField}
                                                className={classes.inputStyle}
                                            />}
                                    </Typography>
                                    <Typography variant={'body1'} component={'span'} className={classes.labelStyle} align="left">
                                        <b style={{ display: 'block' }}>Nome: </b>
                                        {!editNome ? <span onClick={() => setEditNome(true)} className={classes.textStyle}>{values.nome}</span> :
                                            <Field
                                                key="nome"
                                                name="nome"
                                                type="input"
                                                component={TextField} 
                                                className={classes.inputStyle}
                                            />}
                                    </Typography>
                                    <Typography variant={'body1'} component={'span'} className={classes.labelStyle} align="left">
                                        <b style={{ display: 'block' }}>Password: </b>
                                        {!editPassword ? <span onClick={() => { setEditPassword(true); setEditPassword2(true) }} className={classes.textStyle}>*******</span> :
                                            <Field
                                                key="password"
                                                name="password"
                                                type="password"
                                                component={TextField}
                                                className={classes.inputStyle}
                                            />}
                                    </Typography>
                                    <Typography variant={'body1'} component={'span'} className={classes.labelStyle} align="left">
                                        {!editPassword2 ? null :
                                            <div><b style={{ display: 'block' }}>Confirme Password: </b>
                                                <Field
                                                    key="password2"
                                                    name="password2"
                                                    type="password"
                                                    component={TextField}
                                                    className={classes.inputStyle}
                                                /></div>}
                                    </Typography>
                                    <Typography variant={'body1'} component={'span'} className={classes.labelStyle} align="left">
                                        <b style={{ display: 'block' }}>Morada: </b>
                                        {!editMorada ? <span onClick={() => setEditMorada(true)} className={classes.textStyle}>{values.morada}</span> :
                                            <Field
                                                key="morada"
                                                name="morada"
                                                type="input"
                                                component={TextField}
                                                className={classes.inputStyle}
                                            />}
                                    </Typography>
                                    <Typography variant={'body1'} component={'span'} className={classes.labelStyle} align="left">
                                        <b style={{ display: 'block' }}>Distrito: </b>
                                        {!editDistrito ? <span onClick={() => setEditDistrito(true)} className={classes.textStyle}>{values.distrito}</span> :
                                            <Field
                                                key="distrito"
                                                name="distrito"
                                                type="text"
                                                select
                                                helperText="Por favor escolha o distrito"
                                                component={TextField}
                                                className={classes.inputStyle}>
                                                {distritos.map(distrito => (<MenuItem key={distrito.value} value={distrito.value}>{distrito.label}</MenuItem>))}
                                            </Field>}
                                    </Typography>
                                    <Button variant="contained" color="primary" disabled={!dirty || !isValid || isSubmitting} className={classes.buttonStyle} type="submit">Guardar</Button>
                                </ThemeProvider>
                            </Form>
                        )
                    }}
                </Formik >
                <Snackbar open={open} autoHideDuration={2000} onClose={handleClose} message="Alterações Guardadas!" anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}>
                </Snackbar>
            </div >
    );
};

