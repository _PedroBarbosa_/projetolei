import React from 'react';
import { CircularProgress, Paper, makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    }
}));

function Loading(props) {

    const classes = useStyles();

    return (
        <div>
            <Paper className={classes.paper} elevation={2}>
                <CircularProgress />
            </Paper>
        </div>
    );
}

export default Loading;