import React, { useEffect, useState } from 'react';
import { makeStyles, Grid, Typography, ThemeProvider, Paper } from '@material-ui/core';
import { theme } from '../../../theme';
import axios from 'axios';
import Loading from '../Loading';
import { apiURL } from '../../../AxiosConfig';
import { getCurrentUser } from '../../../services/Utils';
import MyProdItem from './MyProdItem';

const useStyles = makeStyles((theme) => ({
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
    topDiv: {
        marginTop: '3vh',
        width: '80vw',
        alignItems: 'center',
        marginBottom: '2vh'
    },
    divStyle: {
        display: 'flex',
        justifyContent: 'center'
    },
    containerCards: {
        minHeight: '30vh',
    }
}));

function MyProdRec(props) {

    const classes = useStyles();
    const [data, setData] = useState();
    const [loading, setLoading] = useState(true);

    var id;
    if (getCurrentUser)
        id = getCurrentUser().id_utilizador

    useEffect(() => {
        axios.get(apiURL + '/produtorecicladoU/' + id)
            .then((res) => {
                setData(res.data);
                setLoading(false);
            }).catch((error) => {
                setData([])
                setLoading(false)
            }).then(() => {
            })
    }, [])


    return (
        loading ? <Loading /> :
            <div className={classes.divStyle}>
                <Grid container direction="column" justify="center" alignItems="center" className={classes.topDiv}>
                    <Grid item xs={12} className={classes.topDiv}>
                        <ThemeProvider theme={theme}>
                            <Typography variant="h5" color="secondary">
                                <Paper className={classes.paper}>
                                    Os meus Produtos reciclados
                                </Paper>
                            </Typography>
                        </ThemeProvider>
                    </Grid>
                    <Grid container justify="center" spacing={3} className={classes.containerCards}>
                        {data.length > 0 ?data.map((values) => <MyProdItem loading={loading} values={values} img={values.img_path} key={values.id_produto_rec} />) :
                        <div className={classes.topDiv}>Não tem nenhum produto reciclado! Vá ao painel de controlo e começe já a sua viagem no mundo dos produtos reciclados! </div>}
                    </Grid>
                </Grid>
            </div>
    );
}

export default MyProdRec;