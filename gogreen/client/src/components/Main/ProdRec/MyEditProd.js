import React, { useState } from 'react';
import { Paper, Grid, makeStyles, Typography, Button, ThemeProvider, Snackbar, Input } from '@material-ui/core';
import { theme } from '../../../theme';
import Axios from 'axios';
import { apiURL } from '../../../AxiosConfig';
import noimg from '../../../images/noimg.png'

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing(3),
        color: theme.palette.text.secondary,
    },
    topDiv: {
        marginTop: '3vh',
        width: '40vw',
    },
    ImgCampanha: {
        height: 'inherit',
        width: '30vw',
        borderRadius: 3,
    }, leftDiv: {
        flex: 1,
        marginRight: '3vw'
    },
    contentDiv: {
        display: 'flex'
    },
    campStatusDiv: {
        flex: 1,
        marginBottom: '5vh'
    },
    titleStyle: {
        marginTop: 10,
        paddingBottom: '2vh',
        marginBottom: '3vh'
    },
    linearBar: {
        width: '25vw',
        height: '1rem',
        display: 'inline-block',
        borderRadius: 20,

    }, buttonsDiv: {
        position: 'relative',
        textAlign: 'right'
    },
    media: {
        height: '40vh',
        width: 'inherit',
    },
    preco: {
        position: 'relative',
        textAlign: 'right',
        backgroundColor: 'whitesmoke',
        padding: 5,

    },
    precoDiv: {
        textAlign: 'right',
        marginBottom: '20px'
    },
    inputStyle: {
        marginTop: 10,
        marginBottom: '3vh',
    },
    saveButton: {
        marginLeft: 10
    },
    tStyle:
    {
        backgroundColor: '#bfbfbf',
        color: 'white',
        marginBottom: 10,
        borderRadius: 2,
        paddingLeft: 10
    }
}));



function MyEditProd(props) {

    /** -------------------------------Hooks --------------------------------------------- */
    const classes = useStyles()
    const [editTitle, setEditTitle] = useState(false);
    const [titleData, setTitleData] = useState(props.values.nome)
    const [editDesc, setEditDesc] = useState(false);
    const [descData, setDescData] = useState(props.values.descricao)
    const [editPrice, setEditPrice] = useState(false);
    const [priceData, setPriceData] = useState(props.values.preco)
    const [open, setOpen] = useState(false)

    const [tempImg, setTempImg] = useState(true)

    let img;
    try {
        img = apiURL + "/campanhaImgUpload/" + props.img;
    } catch (e) {
        setTempImg(false)
    }

    const handleClose = () => {
        setOpen(false)
    }

    const handleTitle = (e) => {
        setTitleData(e.target.value)
    }
    const handleDesc = (e) => {
        setDescData(e.target.value)
    }
    const handlePrice = (e) => {
        setPriceData(e.target.value)
    }

    const titleInput = <div><Input onChange={handleTitle} defaultValue={props.values.nome} className={classes.inputStyle} /><Button className={classes.saveButton} onClick={() => titleUpdate()}>Guardar</Button></div>
    const descInput = <div><Input onChange={handleDesc} defaultValue={props.values.descricao} multiple className={classes.inputStyle} /><Button className={classes.saveButton} onClick={() => descUpdate()}>Guardar</Button></div>
    const priceInput = <div><Input onChange={handlePrice} defaultValue={props.values.preco} type="number" className={classes.inputStyle} /><Button className={classes.saveButton} onClick={() => priceUpdate()}>Guardar</Button></div>

    const titleUpdate = () => {
        console.log(titleData)
        setEditTitle(false)

        const formData = new FormData()

        //if(props.values.nome != titleData)
        formData.append('nome', titleData)

        Axios.put(apiURL + '/produtoreciclado/' + props.values.id_produto_rec, formData)
            .then((res) => {
                setOpen(true)
            }).catch((error) => {
                console.log(error);
            })
    }

    const descUpdate = () => {
        setEditDesc(false)

        const formData = new FormData()

        //if(props.values.nome != titleData)
        formData.append('descricao', descData)

        Axios.put(apiURL + '/produtoreciclado/' + props.values.id_produto_rec, formData)
            .then((res) => {
                setOpen(true)
            }).catch((error) => {
                console.log(error);
            })
    }

    const priceUpdate = () => {
        setEditPrice(false)

        const formData = new FormData()

        //if(props.values.nome != titleData)
        formData.append('preco', priceData)

        Axios.put(apiURL + '/produtoreciclado/' + props.values.id_produto_rec, formData)
            .then((res) => {
                setOpen(true)
            }).catch((error) => {
                console.log(error);
            })
    }

    /**----------------------------------------- Snackbar --------------------------------------------- */

    /** ---------------------------------------- UI ------------------------------------------------ */
    return (
        <div>
            <div style={divStyle}>
                <Grid container spacing={3} className={classes.topDiv} >
                    <Grid item xs={12}>
                        <Paper className={classes.paper}>
                            <Typography variant="h5" component={'span'} className={classes.tStyle} display="block">
                                Editar produto
                            </Typography>
                            {tempImg ? <img src={img} alt="produto" className={classes.media} onError={() => setTempImg(false)} />
                                : <img src={noimg} alt="imagem nao encontrada" className={classes.media} />}
                            <ThemeProvider theme={theme}>
                                <Typography component={'span'} variant={'body2'}>
                                    <div className={classes.campStatusDiv}>
                                        {editTitle ? titleInput : <div onClick={() => setEditTitle(true)}><h2 className={classes.titleStyle} >{titleData}</h2></div>}
                                        <b>descrição: </b> {editDesc ? descInput : <div onClick={() => setEditDesc(true)}><p style={{ marginBottom: '5vh' }}>{descData}</p></div>}
                                    </div>
                                    <b>Adicionado em: </b> {new Date(props.values.data).toLocaleDateString()}
                                </Typography>
                                <div className={classes.precoDiv}>
                                    <Typography component={'span'} variant={'h6'} align="right" color="textPrimary" paragraph>
                                        {editPrice ? priceInput : <div onClick={() => setEditPrice(true)}><b><span className={classes.preco}>{priceData} €</span></b></div>}
                                    </Typography>
                                </div>
                                <div className={classes.buttonsDiv}>
                                    <Button variant="outlined" size="small" color="secondary"
                                        onClick={() => props.handleClose()} style={{ marginRight: '10px' }}>
                                        Fechar
                                    </Button>
                                </div>
                            </ThemeProvider>
                        </Paper>
                    </Grid>
                </Grid>
            </div>
            <Snackbar open={open} autoHideDuration={2000} onClose={handleClose} message="Perfil editado com sucesso." anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}>
            </Snackbar>
        </div>
    );
}

const divStyle = {
    display: 'flex',
    justifyContent: 'center'
}

export default MyEditProd;