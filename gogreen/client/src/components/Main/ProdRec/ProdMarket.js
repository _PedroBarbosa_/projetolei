import React, { useEffect, useState } from 'react';
import { makeStyles, Grid } from '@material-ui/core';
import { theme } from '../../../theme';
import axios from 'axios';
import Loading from '../Loading';
import { apiURL } from '../../../AxiosConfig';
import { getCurrentUser } from '../../../services/Utils';
import ProdItem from './ProdItem';

const useStyles = makeStyles((theme) => ({
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
    topDiv: {
        marginTop: '3vh',
        width: '80vw',
        alignItems: 'center',
    },
    divStyle: {
        display: 'flex',
        justifyContent: 'center'
    }
}));

function ProdMarket(props) {

    const classes = useStyles();
    const [data, setData] = useState();
    const [loading, setLoading] = useState(true);
    
    var id; 
    if(getCurrentUser())
        id = getCurrentUser().id_utilizador


    useEffect(() => {
        axios.get(apiURL + '/produtoreciclado')
            .then((res) => {
                setData(res.data[0]);
                setLoading(false);
            }).catch((error) => {
            }).then(() => {
            })
    }, [])

    

    return (
        loading ? <Loading /> :
            <div className={classes.divStyle}>
                <Grid container className={classes.topDiv} >
                    <Grid container justify="center" spacing={3}>
                        {data.map((values) => !(id == values.id_utilizador) ? <ProdItem loading={loading} values={values} img={values.img_path} key={values.id_produto_rec} /> : null)}
                    </Grid>
                </Grid>
            </div>
    );
}

export default ProdMarket;