import React, { useState, Fragment } from 'react'
import { Button, DialogTitle, DialogActions, makeStyles, CircularProgress, Snackbar } from "@material-ui/core";
import { TextField } from 'formik-material-ui';
import { Formik, Form, Field } from "formik";
import CloseIcon from '@material-ui/icons/Close';
import axios from 'axios';
import { apiURL } from '../../../AxiosConfig';

/** ----------------------------------- ESTILOS ------------------------------------------- */
const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing(3),
        color: theme.palette.text.secondary,
    },
    paperModal: {
        padding: theme.spacing(3),
        color: theme.palette.text.secondary,
        ouline: 'none'
    },
    buttonNav: {
        paddingRight: 20,
        borderRadius: 0

    },
    hr: {
        opacity: '0.3',
        marginTop: 5,
        marginBottom: 5
    },
    menu: {
        marginBottom: 20,
    },
    modal: {
        margin: '3vh',
    },
    buttonModalOK: {
        color: 'white',
        backgroundColor: 'seagreen',
    },
    buttonModalCancel: {
        color: 'gray',

    },
    modalTitle: {
        backgroundColor: 'mediumseagreen',
    },
    modalInput: {
        with: '100%',
        width: '100%',
        marginBottom: 20,
        display: 'block',
        '& .MuiInput-underline:after, ': {
            borderBottomColor: 'green'
        },
        '& label.Mui-focused': {
            color: 'green'
        }
    },
    inputStyle: {
        border: '1px solid green',
        color: 'pink'
    },
    imgPreview: {
        height: '5vh',
        marginTop: 10,
    },
    inputSubmit: {
        marginTop: 10,
        backgroundColor: 'seagreen',
        color: 'white',
        borderRadius: 4,
        padding: '5px 10px 5px 10px',
        border: 'none'
    }
}));

export default function NewProdRecModal(props) {

    /**------------------------------------ HOOKS --------------------------------------------------- */
    const classes = useStyles();

    /** States para o upload de ficheiros */
    const [file, setFile] = useState('');
    const [fileName, setFileName] = useState('Escolher ficheiro');
    const [imgPreview, setImgPreview] = useState();
    const [uploadedFile, setUploadedFile] = useState();
    const [loading, setLoading] = useState(false);
    const [open, setOpen] = useState(false);
    const [imgError, setImgError] = useState(true)
    /** --------------------------------- CONSTANTES ------------------------------------------------ */

    const handleClose = () => {
        setOpen(false)
    }

    /**onChange para o upload do ficheiro */
    const onChange = e => {
        setFile(e.target.files[0]);
        setFileName(e.target.files[0].name);
        var tempFile = e.target.files[0];
        var reader = new FileReader();
        reader.readAsDataURL(tempFile);
        setImgError(false)
        reader.onloadend = (e) => {
            setImgPreview(reader.result);
        }
    }
    /** onSubmit para o upload do ficheiro */
    const onSubmit = async e => {
        setLoading(true);
        //e.preventDefault()
        const formData = new FormData();
        formData.append('file', file);

        try {
            const res = await axios.post(apiURL + '/campanhaImgUpload', formData, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            }).then((res) => {
                setLoading(false)
            });

            const { fileName, filePath } = res.data;
            setUploadedFile({ fileName, filePath });

        } catch (error) {
            //console.log(JSON.stringify(error, null, 2))
        }
    }
    const valsCampanha = {
        nome: '',
        descricao: '',
        preco: 0,
        img_path: "",
    }
    /** ------------------------------------------Funçoes ---------------------------------------------- */

    const handleSubmitCampanha = (values) => {

        if (!imgError) {
            axios.post(apiURL + '/produtoreciclado', {
                nome: values.nome,
                descricao: values.descricao,
                preco: values.preco,
                img_path: fileName,
                data: new Date(),
                id_utilizador: props.id,
            })
                .then((res) => {
                    if (res.status == 200) setOpen(true)
                }).catch((error) => {
                    console.log(error)
                })
            onSubmit()
        }
    }


    /**----------------------------------------- UI --------------------------------------------------- */
    return (
        <div className={classes.modal}>
            <Button onClick={props.closeDialog} style={{ float: 'right' }}>
                <CloseIcon />
            </Button>
            <DialogTitle id="Campanha">Registar Produto</DialogTitle>
            <div className={classes.modal}>
                {/** FORMIK*/}
                <Formik
                    initialValues={valsCampanha}
                    onSubmit={handleSubmitCampanha}
                >
                    {({ dirty, isSubmitting }) => {
                        return (
                            <Form className={classes.formStyle}>
                                <Field
                                    key="nome"
                                    name="nome"
                                    type="input"
                                    label="Nome"
                                    component={TextField}
                                    helperText="Introduza o nome do produto"
                                    required
                                    fullWidth
                                    className={classes.modalInput}
                                />
                                <Field
                                    key="descricao"
                                    name="descricao"
                                    type="textarea"
                                    label="Descricao"
                                    component={TextField}
                                    fullWidth
                                    helperText="Introduza uma breve descrição do produto"
                                    className={classes.modalInput}
                                />
                                <Field
                                    key="preco"
                                    name="preco"
                                    type="number"
                                    label="Preço"
                                    component={TextField}
                                    className={classes.modalInput}
                                />
                                <DialogActions>
                                    <Button autoFocus variant="outlined" onClick={props.closeDialog} className={classes.buttonModalCancel}>
                                        Cancelar
                                        </Button>
                                    <Button type="submit" variant="contained" color="primary" className={classes.buttonModalOK} disabled={!dirty || imgError}>
                                        OK
                                        </Button>
                                </DialogActions>
                            </Form>
                        )
                    }}
                </Formik >
                {/** END FORMIK */}
                <Fragment>
                    <form onSubmit={onSubmit}>
                        <div className={classes.fileDiv}>
                            <input type="file" className="fileInput" id="fileInput" onChange={onChange} />
                        </div>

                        {loading ? <CircularProgress /> : null}
                        
                    </form>
                    {file ? <img src={imgPreview} alt="img preview" className={classes.imgPreview} /> : null}
                    {imgError ? <span style={{color: 'red'}}>Por favor escolha uma imagem!</span> : null}
                </Fragment>
                <Snackbar open={open} autoHideDuration={2000} onClose={handleClose} message="Produto adicionado." anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}>
                </Snackbar>
            </div >
        </div >
    );
}