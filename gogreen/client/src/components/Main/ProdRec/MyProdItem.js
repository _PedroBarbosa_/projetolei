import React, { useState } from 'react';
import { Button, CardActions, CardContent, Typography, Card, makeStyles, ThemeProvider, CircularProgress, Modal } from '@material-ui/core';
import { theme } from '../../../theme';
import axios from 'axios'
import { apiURL } from '../../../AxiosConfig';
import MyEditProd from './MyEditProd';
import SmallWidthUnder500px from '../../MediaQueries/SmallWidthUnder500px';
import noimg from '../../../images/noimg.png'

const { is500 } = <SmallWidthUnder500px />

const useStyles = makeStyles((theme) => ({
    root: {
        minWidth: '15vw',
        maxWidth: '20vw',
        margin: '0.5vw',
        height: 'inherit'
    },
    media: {
        height: '25vh',
        width: 'inherit',
    },
    container: {
        height: 'inherit',
    },
    Nodescription: {
        color: 'lightgray'
    },
    linkStyle: {
        textDecoration: 'none',
        '&:visited, &:link, &:active': {
            textDecoration: 'none',
            color: 'inherit',
        }
    },
    precoStyle: {
        backgroundColor: 'whitesmoke',
        padding: 10,
        borderRadius: 10,
    },
    paperMin: {
        height: 'inherit',
        width: '100%',
    },
}));

const removeProd = (id_prod) => {
    axios.delete(apiURL + '/produtoreciclado/' + id_prod)
        .then((res) => {
            if (res.status == 200) {
                //open snackbar
                setTimeout(() => {
                    window.location.reload(false);
                }, 1000)
            }

            //open sackbar
        }).catch((error) => {
            //open error snaback
        })
}



function MyProdItem(props) {
    const classes = useStyles();
    const [tempImg, setTempImg] = useState(true)

    let img;
    try {
        img = apiURL + "/campanhaImgUpload/" + props.img;
    } catch (e) {
        setTempImg(false)
    }
    const [open, setOpen] = useState(false);
    const handleClose = () => {
        setOpen(false)
    }

    const body = <MyEditProd values={props.values} handleClose={handleClose} img={props.img}/>

    const calcDays = day => {
        const oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
        const firstDate = new Date();
        const secondDate = new Date(day);
        var diffDays = Math.round(Math.abs((firstDate - secondDate) / oneDay));
        return diffDays;
    }

    return (
        props.loading ? <CircularProgress /> :
            <div className={is500 ? classes.paperMin : classes.topDiv}>
                <Card className={classes.root}>
                    {tempImg ? <img src={img} alt="produto" className={classes.media} onError={() => setTempImg(false)} />
                        : <img src={noimg} alt="imagem nao encontrada" className={classes.media} />}
                    <CardContent>
                        <Typography gutterBottom variant="h6" component="h3">
                            {props.values.nome}
                        </Typography>
                        <Typography variant="body2" color="textSecondary" component="p" style={{ marginBottom: '15px' }}>
                            <span className={classes.Nodescription}>{props.values.descricao}</span>
                        </Typography>
                        <Typography variant="body2" color="textPrimary" component="p">
                            <b><span className={classes.precoStyle}>{props.values.preco} €</span></b>
                        </Typography>
                        <Typography variant="body2" color="textPrimary" component="p" align="right">
                            <span className={classes.Nodescription}> há {calcDays(props.values.data)} dias</span>
                        </Typography>
                    </CardContent>
                    <CardActions>
                        <ThemeProvider theme={theme}>
                            <Button size="small" color="secondary" onClick={() => { removeProd(props.values.id_produto_rec) }}>
                                Remover
                            </Button>
                            <Button variant="outlined" size="small" color="primary"
                                onClick={() => setOpen(true)}>
                                Editar
                            </Button>
                        </ThemeProvider>
                    </CardActions>
                </Card>
                <Modal
                    open={open}
                    onClose={handleClose}
                    aria-labelledby="simple-modal-title"
                    aria-describedby="simple-modal-description"
                    onBackdropClick={handleClose}
                >
                    {body}
                </Modal>
            </div>
    );
}

export default MyProdItem;