import React, { useState } from 'react';
import { Button, CardActions, CardContent, Typography, Card, makeStyles, ThemeProvider, CircularProgress, Modal, Snackbar } from '@material-ui/core';
import { theme } from '../../../theme';
import { getCurrentUser } from '../../../services/Utils';
import ProdView from './ProdView'
import axios from 'axios'
import { apiURL } from '../../../AxiosConfig'
import noimg from '../../../images/noimg.png'


const useStyles = makeStyles((theme) => ({
    root: {
        width: '15vw',
        margin: '0.5vw',
        minHeight: '30vh'
    },
    media: {
        maxWidth: '14vw',
        margin: '5px',
        border: '1px solid lightgray'
    },
    Nodescription: {
        color: 'lightgray'
    },
    linkStyle: {
        textDecoration: 'none',
        '&:visited, &:link, &:active': {
            textDecoration: 'none',
            color: 'inherit',
        }
    },
    precoStyle: {
        backgroundColor: 'whitesmoke',
        padding: 10,
        borderRadius: 10,
    }
}));






function ProdItem(props) {
    const classes = useStyles();
    const [open, setOpen] = useState(false);
    const [openSnackbar, setOpenSnackbar] = useState(false)

    const [tempImg, setTempImg] = useState(true)

    const img = apiURL + "/campanhaImgUpload/" + props.img;

    const buyProd = (targetId_ut, nomeProd) => {
        axios.post(apiURL + '/utilizadorNotificacoes/', {
            id_utilizador: targetId_ut,
            titulo: 'Pedido de Compra',
            descricao: 'O utilizador ' + getCurrentUser().nome + ' está a contactar para comprar o produto ' + nomeProd + '. Contacte-o para a troca!',
            data: new Date()
        })
            .then((res) => {
                setOpenSnackbar(true)
            }).catch((error) => {
                //open error snaback
                console.log(error)
            })
    }


    const handleClose = () => {
        setOpen(false)
    }

    const handleCloseSnackbar = () => {
        setOpenSnackbar(false)
    }

    const body = <ProdView values={props.values} buyProd={buyProd} handleClose={handleClose} img={props.img}/>

    const calcDays = day => {
        const oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
        const firstDate = new Date();
        const secondDate = new Date(day);
        var diffDays = Math.round(Math.abs((firstDate - secondDate) / oneDay));
        return diffDays;
    }

    return (
        props.loading ? <CircularProgress /> :
            <div>
                <Card className={classes.root}>
                    {tempImg ? <img src={img} alt="produto" className={classes.media} onError={() => setTempImg(false)} />
                        : <img src={noimg} alt="imagem nao encontrada" className={classes.media} />}
                    <CardContent>
                        <Typography gutterBottom variant="h6" component="h3">
                            {props.values.nome}
                        </Typography>
                        <Typography variant="body2" color="textSecondary" component="p" style={{ marginBottom: '15px' }}>
                            <span className={classes.Nodescription}>{props.values.descricao}</span>
                        </Typography>
                        <Typography variant="body2" color="textPrimary" component="p">
                            <b><span className={classes.precoStyle}>{props.values.preco} €</span></b>
                        </Typography>
                        <Typography variant="body2" color="textPrimary" component="p" align="right">
                            <span className={classes.Nodescription}> há {calcDays(props.values.data)} dias</span>
                        </Typography>
                    </CardContent>
                    <CardActions>
                        <ThemeProvider theme={theme}>
                            <Button variant="outlined" size="small" color="secondary"
                                onClick={() => setOpen(true)}>
                                Ver
                            </Button>
                            <Button variant="contained" size="small" color="primary" style={{ color: 'white' }}
                                onClick={() => buyProd(props.values.id_utilizador, props.values.nome)}>
                                Contactar
                            </Button>
                        </ThemeProvider>
                    </CardActions>
                </Card>
                <Modal
                    open={open}
                    onClose={handleClose}
                    aria-labelledby="simple-modal-title"
                    aria-describedby="simple-modal-description"
                >
                    {body}
                </Modal>
                <Snackbar open={openSnackbar} autoHideDuration={2000} onClose={handleCloseSnackbar} message="Contato enviado!" anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}>
                </Snackbar>
            </div>
    );
}

export default ProdItem;