import React  from 'react';
import { Paper, Grid, makeStyles, Typography, Button, ThemeProvider } from '@material-ui/core';
import { theme } from '../../../theme';
import { apiURL } from '../../../AxiosConfig'


const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing(2),
        color: theme.palette.text.secondary,
        minHeight: '30vh',
    },
    topDiv: {
        marginTop: '3vh',
        width: '40vw',
    },
    contentDiv: {
        display: 'flex'
    },
    titleStyle: {
        paddingBottom: '2vh',
        marginBottom: '3vh'
    }, buttonsDiv: {
        position: 'relative',
        textAlign: 'right'
    },
    img: {
        maxWidth: 'inherit',
        maxHeight: '45vh',
        border: '1px solid black',
        display: 'block',
    },
    preco: {
        position: 'relative',
        textAlign: 'right',
        backgroundColor: 'whitesmoke',
        padding: 5,

    },
    precoDiv: {
        textAlign: 'right',
        marginBottom: '20px'
    }

}));



function ProdView(props) {

    /** -------------------------------Hooks --------------------------------------------- */
    const classes = useStyles()
    const buyProd = props.buyProd;
    const img = apiURL + "/campanhaImgUpload/" + props.img;


    /**----------------------------------------- Snackbar --------------------------------------------- */


    /** ---------------------------------------- UI ------------------------------------------------ */
    return (
        <div>
            <div style={divStyle}>
                <Grid container spacing={3} className={classes.topDiv} >
                    <Grid item xs={12} className={classes.topDiv}>
                        <Paper className={classes.paper}>
                            <img src={img} alt="produto" className={classes.img} />
                            <ThemeProvider theme={theme}>
                                <Typography component={'span'} variant={'body2'}>
                                    <div className={classes.campStatusDiv}>
                                        <h2 className={classes.titleStyle}>{props.values.nome}</h2>
                                        <b>descrição: </b><p style={{ marginBottom: '5vh' }}>{props.values.descricao}</p>
                                    </div>
                                    <b>Adicionado em: </b> {new Date(props.values.data).toLocaleDateString()}
                                </Typography>
                                <div className={classes.precoDiv}>
                                    <Typography component={'span'} variant={'h6'} align="right" color="textPrimary" paragraph>
                                        <b><span className={classes.preco}>{props.values.preco} €</span></b>
                                    </Typography>
                                </div>
                                <div className={classes.buttonsDiv}>
                                    <Button variant="outlined" size="small" color="secondary"
                                        onClick={() => props.handleClose()} style={{ marginRight: '10px' }}>
                                        Fechar
                                    </Button>
                                    <Button variant="contained" size="small" color="primary" style={{ color: 'white' }}
                                        onClick={() => buyProd(props.values.id_utilizador, props.values.nome)}>
                                        Contactar
                                     </Button>
                                </div>
                            </ThemeProvider>
                        </Paper>
                    </Grid>
                </Grid>
            </div>
        </div>
    );
}

const divStyle = {
    display: 'flex',
    justifyContent: 'center'
}

export default ProdView;