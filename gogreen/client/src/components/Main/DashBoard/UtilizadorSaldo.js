import React, { useState, useContext } from 'react';
import { makeStyles, Paper, Grow, DialogTitle, ListItem, ListItemText, List, Dialog, DialogActions, Button } from '@material-ui/core';
import moneyIconSVG from '../../../images/cardOutline.svg'
import Loading from '../Loading';
import axios from 'axios'
import { apiURL } from '../../../AxiosConfig'
import { theme } from '../../../theme'
import LoginContext from '../../../context'


const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing(3),
        color: theme.palette.text.secondary,
        cursor: 'pointer'
    },
    majorIcons: {
        float: 'left',
        height: '10vh',
        display: 'inline',
        marginLeft: '2vw'
    },
    modal: {
        margin: '0px auto'
    },
    secondaryButton: {
        color: 'whitesmoke',
    },
    primaryButton: {
        color: 'forestgreen'
    }
}));

function UtilizadorSaldo(props) {

    const classes = useStyles();
    const loading = props.loading
    const { value } = useContext(LoginContext)
    const saldo = props.saldo;
    const [open, setOpen] = useState(false)
    const [escolhaValor, setEscolhaValor] = useState()

    const handleClose = () => {
        setOpen(false)
    }

    const handleConfirm = () => {
        if (escolhaValor) {
            axios.post(apiURL + '/utilizadorSaldo', {
                id_utilizador: value,
                valor: -escolhaValor
            }).then((res) => {
                console.log(res)
                if (res.status == 200) handleClose()
            }).catch((error) => {

            })
        }
    }

    const sumSaldo = (obj) => {
        var total = 0;
        if (obj) {
            for (var i = 0; i < obj.length; i++) {
                total += obj[i].valor;
            }
        }
        return total;
    }

    const checkSaldo = (valor) => {
        if ((sumSaldo(saldo) - valor) < 0) return true
        else return false
    }

    return (
        loading ? <Loading /> :
            <div>
                <Grow in>
                    <Paper className={classes.paper} elevation={2} onClick={() => setOpen(true)} >
                        <img src={moneyIconSVG} alt="money icon" className={classes.majorIcons} />
                        <div>
                            <h1><p style={{ marginBottom: 20, marginLeft: '10vw' }}><b>Saldo: </b></p></h1>
                            <p style={{ marginBottom: 20, marginLeft: '10vw' }}>{sumSaldo(saldo)} €</p>
                        </div>
                    </Paper>
                </Grow>
                <Dialog onClose={handleClose} aria-labelledby="escolher cupao" open={open}>
                    <DialogTitle id="escolher cupao titulo">Escolha o cupão</DialogTitle>
                    <List>
                        {[5, 10, 15, 20, 50, 100].map((escolha) => (
                            <ListItem button onClick={() => setEscolhaValor(escolha)} key={escolha} disabled={checkSaldo(escolha)} selected={escolhaValor == escolha}>
                                <ListItemText primary={escolha + '€'} />
                            </ListItem>
                        ))}
                    </List>
                    <DialogActions>
                        <Button onClick={handleClose} className={classes.secondary}>
                            Cancelar
                            </Button>
                        <Button onClick={() => handleConfirm()} className={classes.primaryButton}>
                            Confirmar
                            </Button>
                    </DialogActions>
                </Dialog>
            </div >
    );
}

export default UtilizadorSaldo;