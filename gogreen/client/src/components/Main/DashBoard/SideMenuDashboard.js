import React, { useState } from 'react';
import { Button, Paper, makeStyles, Grow, Dialog, Drawer } from '@material-ui/core';
import CampanhaForm from '../Campanha/CampanhaForm';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import ChatIcon from '@material-ui/icons/Chat';
import NewProdRecModal from '../ProdRec/NewProdRecModal';
import NewMarcModal from './NewMarcModal';
import UserChat from './UserChat';


const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing(3),
        color: theme.palette.text.secondary,
    },
    paperModal: {
        padding: theme.spacing(3),
        color: theme.palette.text.secondary,
        ouline: 'none'
    },
    buttonNav: {
        paddingRight: 20,
        borderRadius: 0

    },
    hr: {
        opacity: '0.3',
        marginTop: 5,
        marginBottom: 5
    },
    menu: {
        marginBottom: 20,
    },
    modal: {
        margin: '3vh',
    },
    buttonModalOK: {
        color: 'white',
        backgroundColor: 'seagreen',
    },
    buttonModalCancel: {
        color: 'gray',

    },
    modalTitle: {
        backgroundColor: 'mediumseagreen',
    },
    modalInput: {
        with: '100%',
        width: '100%',
        marginBottom: 20,
        display: 'block',
        '& .MuiInput-underline:after, ': {
            borderBottomColor: 'green'
        },
        '& label.Mui-focused': {
            color: 'green'
        }
    },
    inputStyle: {
        border: '1px solid green',
        color: 'pink'
    },
    iconStyle: {
        height: '16px'
    },
    paperAnchorRight: {
        backgroundColor: '#0B0C10',
        width: '20vw',

    },
}));




function SideMenuDashboard(props) {

    const classes = useStyles();
    const [openCampanha, setOpenCampanha] = useState(false);
    const [openProd, setOpenProd] = useState(false)
    const [openMarc, setOpenMarc] = useState(false)
    const [openChat, setOpenChat] = useState(false)
    const id = props.id || null;


    const handleOpenCampanha = () => {
        setOpenCampanha(true);
    }

    const handleCloseCampanha = () => {
        setOpenCampanha(false);
    }

    const handleOpenProdRec = () => {
        setOpenProd(true)
    }

    const handleCloseProdRec = () => {
        setOpenProd(false)
    }

    const handleCloseMarc = () => {
        setOpenMarc(false)
    }

    const handleOpenMarc = () => {
        setOpenMarc(true)
    }

    const handleOpenChat = () => {
        setOpenChat(true)
    }

    const CampForm = <CampanhaForm id={id} closeDialog={handleCloseCampanha} />
    const ProdForm = <NewProdRecModal id={id} closeDialog={handleCloseProdRec} />
    const MarcForm = <NewMarcModal id={id} closeDialog={handleCloseMarc} />

    return (
        <div>
            <Grow in timeout={2000}>
                <Paper className={classes.paper} elevation={2} >
                    <p className={classes.menu}><b>Menu</b></p>
                    {/**Side menu butao de criação de campanha */}
                    <AddCircleIcon className={classes.iconStyle} /><Button className={classes.buttonNav} onClick={handleOpenCampanha} >Criar Campanha</Button>
                    <Dialog open={openCampanha} onClose={handleCloseCampanha} aria-labelledby="Campanha" disableBackdropClick maxWidth="xl">
                        {CampForm}
                    </Dialog>
                    <hr className={classes.hr} />
                    {/**Side menu butao de criação de um produto reciclado */}
                    <AddCircleIcon className={classes.iconStyle} /><Button className={classes.buttonNav} onClick={handleOpenProdRec}>Criar Produto Rec.</Button>
                    <Dialog open={openProd} onClose={handleCloseProdRec} aria-labelledby="ProdRec" disableBackdropClick maxWidth="xl">
                        {ProdForm}
                    </Dialog>
                    <hr className={classes.hr} />
                    {/** Side menu butao para a marcação de uma recolha */}
                    <AddCircleIcon className={classes.iconStyle} /><Button className={classes.buttonNav} onClick={handleOpenMarc}>Marcar Recolha</Button>
                    <Dialog open={openMarc} onClose={handleCloseMarc} aria-labelledby="Marc" disableBackdropClick maxWidth="xl">
                        {MarcForm}
                    </Dialog>
                    <hr className={classes.hr} />
                    {/**Side Menu butao para as conversas com os funcionarios */}
                    <ChatIcon className={classes.iconStyle} /><Button className={classes.buttonNav} onClick={handleOpenChat}>Conversas</Button>
                    <Drawer anchor={'right'} open={openChat} elevation={10} onClose={() => setOpenChat(false)} variant="temporary" classes={{ paperAnchorRight: classes.paperAnchorRight }}>
                        <UserChat /> 
                    </Drawer>
                </Paper>
            </Grow>
        </div>
    );
}

export default SideMenuDashboard;