import React, { useEffect, useState } from 'react';
import { makeStyles, Grid } from '@material-ui/core';
import { theme } from '../../../theme';
import axios from 'axios'
import UtilizadorSaldo from './UtilizadorSaldo';
import UtilizadorPontos from './UtilizadorPontos';
import { apiURL } from '../../../AxiosConfig';
import Loading from '../Loading';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing(3),
        color: theme.palette.text.secondary,
    },
    majorIcons: {
        float: 'left',
        height: '10vh',
        display: 'inline',
        marginLeft: '2vw'
    }
}));

function HeadUserDashboard(props) {

    const classes = useStyles();
    const [saldo, setSaldo] = useState();
    const [pontos, setPontos] = useState();
    const [loading, setLoading] = useState(true);
    const id = props.id;

    const getSaldo = async (callback) => {
        await setTimeout(() => {
            axios.get(apiURL + '/utilizadorSaldo/' + id)
                .then((res) => {
                    if (res.status == 200) setSaldo(res.data)
                }).catch((error) => {
                    setSaldo(0)
                }).then(() => {
                    callback();
                })
        }, 0)
    }


    const getPontos = async () => {
        await axios.get(apiURL + '/utilizadorPontos/' + id)
            .then((res) => {
                if (res.status == 200) setPontos(res.data);
                setLoading(false)
            }).catch((error) => {
                setPontos(0)
                setLoading(false)
            });
    }


    useEffect(() => {
        getSaldo(getPontos);
    }, [])

    return (
        loading ? <Loading /> :
            <div>
                <Grid container className={classes.root} spacing={1} >
                    <Grid item xs={12} sm={6}>
                        <UtilizadorSaldo saldo={saldo} loading={loading} />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <UtilizadorPontos pontos={pontos} loading={loading} />
                    </Grid>
                </Grid>
            </div >
    );
}

export default HeadUserDashboard;