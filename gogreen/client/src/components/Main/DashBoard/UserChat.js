import React, { useEffect, useState, useContext } from 'react';
import { makeStyles, Typography, CircularProgress } from '@material-ui/core';
import Axios from 'axios';
import { apiURL2, apiURL } from '../../../AxiosConfig';
import LoginContext from '../../../context';
import ChatItem from './ChatItem'

const useStyles = makeStyles((theme) => ({
    main: {
        padding: 10,
    },
    title: {
        color: 'whitesmoke',
        textAlign: 'center',
        verticalAlign: 'middle',
        marginBottom: '2vh',
    },
    noConvo: {
        color: 'whitesmoke',
        textAlign: 'center',
        verticalAlign: 'middle',
        marginBottom: '2vh',
        padding: 10,
        border: '1px solid whitesmoke'
    }
}))


function UserChat(props) {

    const classes = useStyles()

    const [loading, setLoading] = useState(true)
    const [nomes, setNomes] = useState([])
    const { value } = useContext(LoginContext)

    const getChats = () => {
        Axios.get(apiURL2 + '/funcionariosDMs/' + value)
            .then((res) => {
                res.data.forEach((conversa, i) => setTimeout(() => getNames(conversa.id_funcionario), 50 * i))
            }).catch((error) => {
                setLoading(false)
            })
    }

    const getNames = (idfunc) => {
        Axios.get(apiURL + '/funcionarios/' + idfunc)
            .then((res) => {
                const temp = [res.data.nomeFuncionario,idfunc]
                setNomes(prevstate => [...prevstate, temp])
            }).catch((error) => {
            })
        setLoading(false)

    }

    useEffect(() => {
        getChats()
    }, [])


    return (
        loading ? <CircularProgress /> :
            <div className={classes.main}>
                <div className={classes.title}>
                    <Typography variant="subtitle1" component={'div'}>
                        <b>Conversas</b>
                    </Typography>
                </div>
                <div>
                    {/** entry[0] = nomeFuncionario | entry[1] = idFuncionario */}
                    {nomes.length > 0 ? nomes.map(entry => <ChatItem values={entry} key={entry[1]} /> ) : <div className={classes.noConvo}>Sem conversas disponiveis</div>}
                </div>
            </div>
    );
}

export default UserChat;