import React from 'react';
import { Paper, makeStyles, Button } from '@material-ui/core';
import camapanhaPNG from '../../../images/campanha.png';

import { Link } from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
        flexGrow: 1,
        height: 'inherit'
    },
    recolhaIMG: {
        height: '15vh',
        marginBottom: '5vh'

    },
    linkStyle: {
        textDecoration: 'none',
        '&:visited, &:link, &:active': {
            textDecoration: 'none',
            color: 'inherit',
        },
        height: 'inherit',
        width: 'inherit'
    },
    subDiv: {
        height: 'inherit',
        width: 'inherit'
    }
}));

function CampanhaButtonDashBoard(props) {

    const classes = useStyles();

    return (
        <div className={classes.subDiv}>
            <Button className={classes.subDiv}>
                <Link to="/minhasCampanhas" className={classes.linkStyle}>
                    <Paper className={classes.paper}>
                        <img src={camapanhaPNG} alt="recolha Icon" className={classes.recolhaIMG} />
                        <div>
                            <h3>As minhas Campanhas</h3>
                        </div>
                    </Paper>
                </Link>
            </Button>
        </div>
    );
}

export default CampanhaButtonDashBoard;