import React, { useEffect, useContext, useState, useRef } from 'react';
import { makeStyles, IconButton, Button, CircularProgress, Typography } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import Axios from 'axios';
import LoginContext from '../../../context'
import { apiURL2 } from '../../../AxiosConfig'
import { Formik, Form, Field } from 'formik';
import { TextField } from 'formik-material-ui'
import { theme } from '../../../theme'

const useStyles = makeStyles((theme) => ({
    window: {
        position: 'fixed',
        bottom: '0px',
        right: '21vw',
        height: 285,
        backgroundColor: 'white',
        width: 300,
        border: '1px solid rgba(29,49,91,.3)',
        borderRadius: '10px 10px 0px 0px'
    },
    whidowClosed: {
        backgroundColor: 'blue',
        padding: 5,
        color: 'white',
        fontWeight: 'bold',
        fontSize: '14px',
        clear: 'both'
    },
    header: {
        backgroundColor: 'rgba(250,250,250,0.5)',
        color: 'black',
        padding: '10px 10px 5px 10px',
        paddingLeft: '20px',
        boxShadow: '0px 1px 1px rbga(0,0,0,0.15), 0px 2px 2px rbga(0,0,0,0.15),0px 4px 4px rbga(0,0,0,0.15),0px 8px 8px rbga(0,0,0,0.15)',
        position: 'relative',
        borderRadius: '10px 10px 0px 0px',
        borderBottom: '1px solid #cccccc'
    },
    closeButton: {
        position: 'absolute',
        color: 'black',
        top: '-10px',
        right: '-10px',
        '&hover': {
            position: 'absolute',
            background: 'gray',
            color: 'black',
            top: '-10px',
            right: '-10px',
        }
    },
    inputDiv: {
        position: 'absolute',
        bottom: 0,
        height: 30,
        margin: '0px auto',
        width: 'inherit',
        padding: '5px 0px 5px 5px',
    },
    inputText: {
        marginRight: '15px'
    },
    textInputDiv: {
        display: 'inline',
    },
    inputButton: {
        alignSelf: 'flex-end',
        backgroundColor: '#557A95'
    },
    conversation: {
        padding: 10,
        overflowY: 'auto',
        height: 190
    },
    myMsg: {
        padding: 5,
        backgroundColor: 'lightgrey',
        color: 'black',
        borderRadius: 5,
        width: '50%',
        overflowWrap: 'break-word',
        marginBottom: 5,
        display: 'block'
    },
    otherMsg: {
        padding: 5,
        backgroundColor: 'gray',
        color: 'whitesmoke',
        borderRadius: 5,
        width: '50%',
        overflowWrap: 'break-word',
        marginBottom: 10,
        display: 'block',
        marginLeft: 'auto',
        marginRight: 0
    },
    textRightAlign: {
        display: 'block',
        marginBottom: 5,

    },
    textLeftAlign: {
        textAlign: 'left',
        display: 'block',
        marginBottom: 5
    }
}))

function ChatWindow(props) {

    const classes = useStyles()
    const { value } = useContext(LoginContext)
    const [conversation, setConversation] = useState([])
    const [loading, setLoading] = useState(true)
    const [intervalValue, setIntervalValue] = useState()

    const messagesEndRef = useRef(null)

    const scrollToBottom = () => {
        messagesEndRef.current.scrollIntoView({ behavior: "smooth" })
      }

    const getConvo = () => {
        Axios.get(apiURL2 + '/getMsgs/' + value + '/' + props.idfunc)
            .then((res) => {
                setConversation(res.data)
                setLoading(false)
                updateConvo()
                scrollToBottom()
            }).catch((error) => {
                console.log(error)
            })
    }

    const updateConvo = () => {
        const url = apiURL2 + '/unreadMessages/' + value + '/' + props.idfunc;
        var clock = setInterval(() => {
            Axios.get(url)
                .then((res) => {
                    if (res.data.length > 0) {
                        console.log(res.data)
                        readMessages()
                        res.data.forEach(element => {
                            setConversation(prevstate => [...prevstate, element])
                        });
                        scrollToBottom()
                    }
                }).catch((error) => {
                    console.log(error)
                })
        }
            , 3000)
        setIntervalValue(clock)
    }

    const readMessages = () => {
        Axios.put(apiURL2 + '/unreadMessages', {
            id_remetente: value,
            id_destino: props.idfunc
        }).then((res) => { }).catch((error) => { console.log("Ocorreu um erro") })
    }

    const handleSubmit = (values, { resetForm }) => {
        Axios.post(apiURL2 + '/unreadMessages', {
            id_remetente: value,
            id_destino: props.idfunc,
            mensagem: values.mensagem
        })
            .then((res) => {

            }).catch((error) => {

            })
        resetForm()
    }

    useEffect(() => {
        getConvo()
    }, [])

    useEffect(scrollToBottom, [conversation]);

    return (
        !props.open ? null :
            <div className={classes.window}>
                <div className={classes.header}>
                    <Typography variant="subtitle2" component={'span'}><b>{props.nome}</b></Typography>
                    <IconButton onClick={() => { props.close(); clearInterval(intervalValue) }} className={classes.closeButton}><CloseIcon fontSize="small" /></IconButton>
                </div>
                <div className={classes.conversation}>
                    {loading ? <Typography variant="caption" style={{ fontSize: '10px' }} component={'span'}>Sem menseagens. Envie uma mensagem para esclarecer qualquer duvida!</Typography> :
                        conversation.map((element) =>
                            element.id_remetente === value ?
                                <div className={classes.textLeftAlign} key={element.data}>
                                    <span className={classes.myMsg}>{element.mensagem}</span>
                                </div> :
                                <div className={classes.textRightAlign} key={element.data}>
                                    <span className={classes.otherMsg}>{element.mensagem}</span>
                                </div>

                        )}
                    <div style={{ float: "left", clear: "both" }} ref={messagesEndRef}>
                    </div>
                </div>
                <div className={classes.inputDiv}>
                    <Formik
                        initialValues={{ mensagem: '' }}
                        onSubmit={handleSubmit}
                    >
                        {({ dirty, setFieldValue }) => {
                            return (
                                <Form>
                                    <Field
                                        name="mensagem"
                                        type="text"
                                        placeholder="Enviar mensagem.."
                                        component={TextField}
                                        className={classes.inputText}
                                        disabled={false}
                                        autoComplete="off"
                                    />
                                    <Button type="submit" variant="contained" size="small" disabled={!dirty} className={classes.inputButton}>Enviar</Button>
                                </Form>
                            )
                        }
                        }</Formik>
                </div>
            </div>
    );
}

export default ChatWindow;