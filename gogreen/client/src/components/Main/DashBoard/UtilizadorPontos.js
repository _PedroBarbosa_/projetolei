import React, { useState, useContext } from 'react';
import { makeStyles, Paper, Grow, Dialog, DialogTitle, List, ListItemText, ListItem, DialogActions, Button, CircularProgress } from '@material-ui/core';
import pointIconSVG from '../../../images/heartcircleoutline.svg';
import Loading from '../Loading';
import axios from 'axios'
import { apiURL } from '../../../AxiosConfig'
import LoginContext from '../../../context';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing(3),
        color: theme.palette.text.secondary,
        cursor: 'pointer'
    },
    majorIcons: {
        float: 'left',
        height: '10vh',
        display: 'inline',
        marginLeft: '2vw'
    },
    secondaryButton: {
        color: 'whitesmoke',
    },
    primaryButton: {
        color: 'forestgreen'
    }
}));

function UtilizadorPontos(props) {

    const classes = useStyles();
    const { value } = useContext(LoginContext)
    const loading = props.loading;
    const pontos = props.pontos;
    const [open, setOpen] = useState(false);
    const [escolhaValor, setEscolhaValor] = useState()
    const [submitting, setSubmitting] = useState(false)

    const handleClose = () => {
        setOpen(false)
    }

    const handleConfirm = () => {
        setSubmitting(true)
        if (escolhaValor) {
            axios.post(apiURL + '/utilizadorPontos', {
                id_utilizador: value,
                data: new Date().toISOString(),
                numPontos: -escolhaValor
            }).then((res) => {
                addSaldo(escolhaValor)
            }).catch((error) => {

            })
        }
    }

    const addSaldo = async (valor) => {
        await axios.post(apiURL + '/utilizadorSaldo', {
            id_utilizador: value,
            data_adicicao: new Date().toISOString(),
            valor: valor / 100
        }).then((res) => {
            if (res.status == 200) handleClose()
            setSubmitting(false)
        }).catch((error) => {

        })
    }

    const sumPontos = (obj) => {
        var total = 0;
        if (obj) {
            obj.forEach(element => {
                total += element.numPontos;
            });
        }
        return total;
    }

    const checkPontos = (valor) => {
        if ((sumPontos(pontos) - valor) < 0) return true
        else return false
    }

    return (
        loading ? <Loading /> :
            <div>
                <Grow in>
                    <Paper className={classes.paper} elevation={2} onClick={() => setOpen(true)}>
                        <img src={pointIconSVG} alt="points icon" className={classes.majorIcons} />
                        <div>
                            <h1><p style={{ marginBottom: 20, marginLeft: '10vw' }}><b>Pontos: </b></p></h1>
                            <p style={{ marginBottom: 20, marginLeft: '10vw' }}>{sumPontos(pontos)}</p>
                        </div>
                    </Paper>
                </Grow>
                <Dialog onClose={handleClose} aria-labelledby="escolher cupao" open={open}>
                    <DialogTitle id="escolher cupao titulo">Troque pontos por cupões</DialogTitle>
                    <List>
                        {[500, 1000, 1500, 2000, 5000, 10000].map((escolha) => (
                            <ListItem button onClick={() => setEscolhaValor(escolha)} key={escolha} disabled={checkPontos(escolha)} selected={escolhaValor == escolha}>
                                <ListItemText primary={escolha + ' Pontos -> ' + escolha / 100 + ' €'} />
                            </ListItem>
                        ))}
                    </List>
                    {submitting ? <CircularProgress /> :<DialogActions>
                        <Button onClick={handleClose} className={classes.secondary}>
                            Cancelar
                            </Button>
                        <Button onClick={() => handleConfirm()} className={classes.primaryButton}>
                            Confirmar
                            </Button>
                    </DialogActions>}
                </Dialog>
            </div>
    );
}

export default UtilizadorPontos;