import React, { useState, useEffect, useContext } from 'react'
import { Button, DialogTitle, DialogActions, makeStyles, CircularProgress, Snackbar, MenuItem } from "@material-ui/core";
import { TextField } from 'formik-material-ui';
import { Formik, Form, Field } from "formik";
import CloseIcon from '@material-ui/icons/Close';
import axios from 'axios';
import { apiURL } from '../../../AxiosConfig';
import LoginContext from '../../../context'
import { getCurrentUser } from '../../../services/Utils'
/** ----------------------------------- ESTILOS ------------------------------------------- */
const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing(3),
        color: theme.palette.text.secondary,
    },
    paperModal: {
        padding: theme.spacing(3),
        color: theme.palette.text.secondary,
        ouline: 'none'
    },
    buttonNav: {
        paddingRight: 20,
        borderRadius: 0

    },
    hr: {
        opacity: '0.3',
        marginTop: 5,
        marginBottom: 5
    },
    menu: {
        marginBottom: 20,
    },
    modal: {
        margin: '3vh',
        width: '50vw',
    },
    buttonModalOK: {
        color: 'white',
        backgroundColor: 'seagreen',
    },
    buttonModalCancel: {
        color: 'gray',

    },
    modalTitle: {
        backgroundColor: 'mediumseagreen',
    },
    modalInput: {
        width: '100px',
        marginRight: '5vw',
        '& .MuiInput-underline:after, ': {
            borderBottomColor: 'green'
        },
        '& label.Mui-focused': {
            color: 'green'
        }
    },
    inputStyle: {
        border: '1px solid green',
        color: 'pink'
    },
    imgPreview: {
        height: '5vh',
        marginTop: 10,
    },
    inputSubmit: {
        marginTop: 10,
        backgroundColor: 'seagreen',
        color: 'white',
        borderRadius: 4,
        padding: '5px 10px 5px 10px',
        border: 'none'
    },
    row: {
        padding: '5px',
        verticalAlign: 'middle',
        borderRaidus: 5,
        display: 'block'
    },
    icons: {
        marginRight: 5
    },
    confirmbutton: {
        backgroundColor: 'forestgreen'
    }
}));

export default function NewMarcModal(props) {

    /**------------------------------------ HOOKS --------------------------------------------------- */
    const classes = useStyles();
    const { value } = useContext(LoginContext);
    const [loading, setLoading] = useState(true);
    const [open, setOpen] = useState(false);
    const [openError, setOpenError] = useState(false);
    const [mats, setMats] = useState();
    const [cats, setCats] = useState()
    const myDist = getCurrentUser().distrito
    const [userInputs, setUserInputs] = useState([{ mat: '', cat: '', qt: '' }])
    const qts = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    const [pontos, setPontos] = useState(0)


    /** --------------------------------- CONSTANTES ------------------------------------------------ */

    const getRecs = () => {
        axios.get(apiURL + '/materiais')
            .then((res) => {
                setMats(res.data[0])
                getCats()
            }).catch((error) => {

            })

    }

    const getCats = async () => {
        await axios.get(apiURL + '/categorias')
            .then((res) => {
                setCats(res.data[0])
                setLoading(false)
            }).catch((error) => {

            })
    }

    useEffect(getRecs, [])

    const handleInputChange = (e, index) => {
        const { name, value } = e.target
        const list = [...userInputs]
        list[index][name] = value
        setUserInputs(list)
    }

    const handleRemoveClick = index => {
        const list = [...userInputs];
        list.splice(index, 1);
        setUserInputs(list);
    }

    const addHandleClick = () => {
        setUserInputs([...userInputs, { mat: '', cat: '', qt: '' }])
    }

    /** ------------------------------------------Funçoes ---------------------------------------------- */

    const handleMarcSubmit = () => {
        const getFunc = () => axios.get(apiURL + '/funcionariosDist/' + myDist)
            .then((res) => {
                insRec(res.data)
            }).catch((error) => {
                setOpenError(true)
            })
        getFunc()

        const insRec = (idfunc) => axios.post(apiURL + '/recolhas', {
            id_utilizador: value,
            id_funcionario: idfunc,
            data: new Date()
        }).then((res) => {
            const idRec = res.data[0].id_recolha;
            insMatsRec(idRec)
        }).catch((error) => {
            console.log(error)
        })

        const insMatsRec = (idRec) => {
            userInputs.forEach((element, i) => {
                setTimeout(() => {
                    axios.post(apiURL + '/recolhasMat', { id_recolha: idRec, line: i + 1, quantidade: element.qt, Nome_residuo: element.mat, cat: element.cat })
                        .then((res) => {
                            if(res.status == 200) setOpen(true)
                        }).catch((error) => {
                            console.log(error)
                        })
                }, 100 * i)
            });
        }
    }       /** const res = axios.post(apiURL + '/recolhas', {
            id_utilizador: value,
            id_funcionario: asdasd,
            data: new Date(),
        }).then((res) => {
            if (res.status == 200) setOpen(true)
        }).catch((error) => {
        }) */


    const calculateAllPoints = () => {
        let total = 0;
        userInputs.forEach(element => {
            let temp = 0;
            switch (element.cat) {
                case 'Eletro':
                    temp = 5
                    break
                case 'Metal' || 'Plastico':
                    temp = 2
                    break
                case 'Papel':
                    temp = 1
                    break
                case 'Vidro':
                    temp = 3
                    break
                default:
                    temp = 0
                    break
            }
            total += temp * element.qt
        })
        setPontos(total)
    }


    /**----------------------------------------- UI --------------------------------------------------- */
    return (
        loading ? <CircularProgress /> :
            <div className={classes.modal}>
                <Button onClick={props.closeDialog} style={{ float: 'right' }}>
                    <CloseIcon />
                </Button>
                <DialogTitle id="Campanha">Marcar Recolha</DialogTitle>
                <div className={classes.content}>
                    <Formik
                        initialValue={{ mat: '', cat: '', qt: '' }}>
                        <Form>
                            {userInputs.map((x, i) => {
                                return (
                                    <div className={classes.row} key={i}>
                                        <Field
                                            name="cat"
                                            select
                                            label="Categoria"

                                            component={TextField}
                                            value={x.cat}
                                            className={classes.modalInput}
                                            onChange={e => handleInputChange(e, i)}>
                                            {cats.map(element => (<MenuItem key={element.cat} value={element.cat}>{element.cat}</MenuItem>))}
                                        </Field>
                                        <Field
                                            name="mat"

                                            select
                                            label="Material"
                                            component={TextField}
                                            value={x.mat}
                                            className={classes.modalInput}
                                            onChange={e => handleInputChange(e, i)}>
                                            {x.cat == 'Eletro' ? mats.map(element => element.Nome_residuo == 'Domestico' ? (<MenuItem key={element.Nome_residuo} value={element.Nome_residuo}>{element.Nome_residuo}</MenuItem>) : null) : mats.map(element => element.Nome_residuo !== 'Domestico' ? (<MenuItem key={element.Nome_residuo} value={element.Nome_residuo}>{element.Nome_residuo}</MenuItem>) : null)}
                                        </Field>
                                        <Field
                                            name="qt"
                                            label="Quantidade"
                                            select

                                            component={TextField}
                                            value={x.qt}
                                            className={classes.modalInput}
                                            onChange={e => { handleInputChange(e, i); calculateAllPoints(); }}>
                                            {qts.map(value => (<MenuItem key={value} value={value}>{value}</MenuItem>))}
                                        </Field>
                                        <div style={{ marginTop: '5px' }}>
                                            {userInputs.length == i + 1 && <Button variant="contained" disableElevation className={classes.icons} onClick={addHandleClick}>+</Button>}
                                            {userInputs.length > 1 && <Button variant="outlined" className={classes.icons} onClick={() => handleRemoveClick(i)}>-</Button>}
                                        </div>

                                    </div>
                                )
                            })}
                            <div>
                                <DialogActions>
                                    <Button variant="outlined" onClick={props.closeDialog}>Cancelar</Button>
                                    <Button variant="contained" color="primary" disabled={pontos < 20} className={classes.confirmbutton} onClick={handleMarcSubmit}>Confirmar</Button>
                                </DialogActions>
                            </div>
                        </Form>
                    </Formik>
                </div >
                <Snackbar open={open} autoHideDuration={2000} onClose={props.closeDialog} message="Marcação registada!" anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}>
                </Snackbar>
                <Snackbar open={openError} autoHideDuration={2000} onClose={props.closeDialog} message="Não foi possivel efetuar marcação! Verifique se encontra no distrito correcto" anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}>
                </Snackbar>
            </div >
    );
}