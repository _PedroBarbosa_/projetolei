import React from 'react';
import { Paper, makeStyles, Button } from '@material-ui/core';
import { Link } from 'react-router-dom';
import prodRec from '../../../images/prodRec.png'


const useStyles = makeStyles((theme) => ({
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
        flexGrow: 1,
        height: 'inherit',
    },
    recolhaIMG: {
        height: '10vh',
        marginBottom: '5vh'

    },
    linkStyle: {
        textDecoration: 'none',
        '&:visited, &:link, &:active': {
            textDecoration: 'none',
            color: 'inherit',
        },
        height: 'inherit',
        width: 'inherit'
    },
    subDiv: {
        height: 'inherit',
        width: 'inherit'
    }
}));

function ProdutoRecDashboard(props) {

    const classes = useStyles();

    return (
        <div className={classes.subDiv}>
            <Button className={classes.subDiv}>
                <Link to="/verMeusProdutos" className={classes.linkStyle}>
                    <Paper className={classes.paper}>
                        <img src={prodRec} alt="recolha Icon" className={classes.recolhaIMG} />
                        <div>
                            <h3>Os meus Produtos Reciclados</h3>
                        </div>
                    </Paper>
                </Link>
            </Button>
        </div >
    );
}

export default ProdutoRecDashboard;