import React, { useState } from 'react';
import { Typography, makeStyles, Button } from '@material-ui/core';
import ChatWindow from './ChatWindow';

const useStyles = makeStyles((theme) => ({
    container: {
        color: 'whitesmoke',
        border: '1px solid whitesmoke',
        padding: '10px 10px 0px 10px',
        margin: '3px 0px 0px 0px',
        textAlign: 'left',
        width: '15vw',
        '&:hover': {
            color: '#0B0C10',
            backgroundColor: 'whitesmoke',
            border: '1px solid whitesmoke',
            padding: '10px 10px 0px 10px',
            margin: '3px 0px 0px 0px',
            textAlign: 'left',
            width: '15vw',
        }
    },
    instruction: {
        opacity: 0.5,
        fontSize: 8,
        color: 'grey'
    },
}))




function ChatItem(props) {

    const classes = useStyles()
    const [openChat, setOpenChat] = useState(false)

    const closeWindowChat = () => {
        setOpenChat(false)
    }

    const openWindowChat = () => {
        setOpenChat(true)
    }

    return (
        <div>
            <Button onClick={openWindowChat}><div className={classes.container}>
                <Typography variant="subtitle1" component={'span'} align="left">
                    {props.values[0]}
                </Typography>
                <div>
                    <Typography variant="body2" component={'span'} className={classes.instruction}>
                        Clique aqui para visualizar a conversa.
                </Typography>
                </div>
            </div>
            </Button>
            <div>
                {openChat && <ChatWindow nome={props.values[0]} idfunc={props.values[1]} open={openChat} close={closeWindowChat} />}
            </div>
        </div>
    );
}

export default ChatItem;