import React, { useContext } from 'react'
import ContextLogin from '../../../context'
import { makeStyles, Grid } from '@material-ui/core';
import HeadUserDashboard from './HeadUserDashboard'
import { theme } from '../../../theme';
import {getCurrentUser } from '../../../services/Utils';
import IdError from './../IdError';
import SideMenuDashboard from './SideMenuDashboard';
import RecolhasButtonDashboard from './RecolhasButtonDashboard';
import ProdutoRecDashboard from './ProdutoRecDashboard';
import CampanhaButtonDashBoard from './CamanhaButtonDashBoard';

/**Material UI styling && Inline Styling */
const useStyles = makeStyles((theme) => ({
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
    topDiv: {
        marginTop: '3vh',
        width: '80vw',
        alignItems: 'center'
    },
    subDivs: {
        height: 'inherit',
        width: 'inherit'
    },
    subDivContainer: {

        flexGrow: 1,
    }
}));

export default function UserDashboard() {

    /**Hooks */
    const { value } = useContext(ContextLogin);
    const classes = useStyles();
    var id;
    if (getCurrentUser())
        id = getCurrentUser().id_utilizador;

    /**UI */

    //está logado (tem id)
    return (
        !value ? <IdError /> :
            <div style={divStyle}>
                <Grid container spacing={3} className={classes.topDiv} >
                    <Grid item xs={12}>
                        <HeadUserDashboard id={id} />
                    </Grid>
                    {/** ----------------------- Subdivs--------------------------------------*/}
                    <Grid container spacing={1} className={classes.subDivContainer}>
                        <Grid item xs={12} sm={3} className={classes.subDivs}>
                            <SideMenuDashboard id={id} />
                        </Grid>
                        <Grid item xs={12} sm={9} className={classes.subDivs}>
                            <Grid container spacing={2} className={classes.subDivs} >
                                <Grid item xs={4} className={classes.subDivs}>
                                    <RecolhasButtonDashboard />
                                </Grid>
                                <Grid item xs={4} className={classes.subDivs}>
                                    <ProdutoRecDashboard />
                                </Grid>
                                <Grid item xs={4} className={classes.subDivs}>
                                    <CampanhaButtonDashBoard />
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </div>
    )

}

const divStyle = {
    display: 'flex',
    justifyContent: 'center'
}