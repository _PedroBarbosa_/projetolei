import React, { useState } from 'react';
import { Button, CardActions, CardContent, Typography, Card, makeStyles, ThemeProvider, CircularProgress, Snackbar } from '@material-ui/core';
import { theme } from '../../../theme';
import axios from 'axios'
import { apiURL } from '../../../AxiosConfig';
import { Link } from 'react-router-dom';
import noimg from '../../../images/noimg.png'


const useStyles = makeStyles((theme) => ({
    root: {
        width: '15vw',
        margin: '0.5vw',
        minHeight: '30vh'
    },
    media: {
        height: '25vh',
        width: 'inherit',
    },
    Nodescription: {
        color: 'lightgray'
    },
    linkStyle: {
        textDecoration: 'none',
        '&:visited, &:link, &:active': {
            textDecoration: 'none',
            color: 'inherit',
        }
    },
    removeButton: {
        color: "red"
    }
}));

function MyCampanhaItem(props) {
    const classes = useStyles();
    const [open, setOpen] = useState()

    const [tempImg, setTempImg] = useState(true)
    const img = apiURL + "/campanhaImgUpload/" + props.img;

    const removeCampanha = () => {
        axios.delete(apiURL + '/campanha/' + props.values.id_campanha)
            .then((res) => {
                if (res.status == 200) setOpen(true)
            }).catch((error) => {
                console.log(error)
            })
    }

    const handleClose = () => {
        setOpen(false)
    }

    const cutDescription = () => {

        var res = props.values.descricao;

        if (!res)
            return "Sem descrição"

        if (res.length > 50)
            res = res.slice(50) + "..."

        return res
    }

    return (

        props.loading ? <CircularProgress /> :
            <div>
                <Card className={classes.root}>
                    {tempImg ? <img src={img} alt="produto" className={classes.media} onError={() => setTempImg(false)} />
                        : <img src={noimg} alt="imagem nao encontrada" className={classes.media} />}
                    <CardContent>
                        <Typography gutterBottom variant="h6" component="h3">
                            {props.values.nome}
                        </Typography>
                        <Typography variant="body2" color="textSecondary" component="p">
                            {props.values.descricao ? cutDescription() : <span className={classes.Nodescription} >{cutDescription()}</span>}
                        </Typography>
                    </CardContent>

                    <CardActions>
                        <ThemeProvider theme={theme}>
                            <Button size="small" color="primary">
                                <Link onClick={() => {
                                    sessionStorage.setItem('id_camp', props.values.id_campanha);
                                }} to="/VerCampanha" className={classes.linkStyle}>
                                    Ver</Link>
                            </Button>
                            <Button variant="outlined" size="small" className={classes.removeButton}
                                onClick={removeCampanha}>
                                Remover
                            </Button>
                        </ThemeProvider>
                    </CardActions>
                </Card>
                <Snackbar open={open} autoHideDuration={2000} onClose={handleClose} message="Campanha removida!" anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}>
                </Snackbar>
            </div>
    );
}

export default MyCampanhaItem;