import React, { Fragment, useState } from 'react';
import { makeStyles, CircularProgress } from '@material-ui/core';
import axios from 'axios';
import { apiURL } from '../../../AxiosConfig';


const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    paper: {

        padding: theme.spacing(3),
        color: theme.palette.text.secondary,
    },
    paperModal: {
        padding: theme.spacing(3),
        color: theme.palette.text.secondary,
        ouline: 'none'
    },
    buttonNav: {
        paddingRight: 20,
        borderRadius: 0

    },
    hr: {
        opacity: '0.3',
        marginTop: 5,
        marginBottom: 5
    },
    menu: {
        marginBottom: 20,
    },
    modal: {
        margin: '3vh',
    },
    buttonModalOK: {
        color: 'white',
        backgroundColor: 'seagreen',
    },
    buttonModalCancel: {
        color: 'gray',

    },
    modalTitle: {
        backgroundColor: 'mediumseagreen',
    },
    modalInput: {
        with: '100%',
        width: '100%',
        marginBottom: 20,
        display: 'block',
        '& .MuiInput-underline:after, ': {
            borderBottomColor: 'green'
        },
        '& label.Mui-focused': {
            color: 'green'
        }
    },
    inputStyle: {
        border: '1px solid green',
        color: 'pink'
    },
    imgPreview: {
        height: '5vh',
        marginTop: 10,
    },
    inputSubmit: {
        marginTop: 10,
        backgroundColor: 'seagreen',
        color: 'white',
        borderRadius: 4,
        padding: '5px 10px 5px 10px',
        border: 'none'
    }
}));



function FileUploadCampanhaComponent(props) {

    /** States para o upload de ficheiros */
    const [file, setFile] = useState('');
    const [fileName, setFileName] = useState('Escolher ficheiro');
    const [imgPreview, setImgPreview] = useState();
    const [uploadedFile, setUploadedFile] = useState();
    const [loading, setLoading] = useState(false);

    /**onChange para o upload do ficheiro */
    const onChange = e => {

        setFile(e.target.files[0]);
        setFileName(e.target.files[0].name);
        var tempFile = e.target.files[0];
        var reader = new FileReader();
        reader.readAsDataURL(tempFile);

        reader.onloadend = (e) => {
            setImgPreview(reader.result);
        }
    }

    /** onSubmit para o upload do ficheiro */
    const onSubmit = async e => {
        setLoading(true);
        e.preventDefault()

        const formData = new FormData();
        formData.append('file', file);

        try {
            const res = await axios.post(apiURL + '/campanhaImgUpload', formData, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            }).then((res) => {
                setLoading(false)
            });

            const { fileName, filePath } = res.data;
            setUploadedFile({ fileName, filePath });

        } catch (error) {
            console.log(JSON.stringify(error, null, 2))

        }
    }

    const classes = useStyles();
    return (
        <Fragment>
            <form onSubmit={onSubmit}>
                <div className={classes.fileDiv}>
                    <input type="file" className="fileInput" id="fileInput" onChange={onChange} />
                </div>

                {loading ? <CircularProgress /> : null}
                <input type="submit" value="Carregar" className={classes.inputSubmit} />

            </form>
            {file ? <img src={imgPreview} alt="img preview" className={classes.imgPreview} /> : null}
        </Fragment>
    );
}

export default FileUploadCampanhaComponent;