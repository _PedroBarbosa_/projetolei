import React, { useEffect, useState } from 'react';
import { Paper, Grid, makeStyles, LinearProgress, Typography, Button, ThemeProvider, Snackbar } from '@material-ui/core';
import axios from 'axios';
import { apiURL } from '../../../AxiosConfig';
import Loading from '../Loading';
import { theme } from '../../../theme';
import { Link } from 'react-router-dom';
import { getCurrentUser, getToken } from '../../../services/Utils';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing(3),
        color: theme.palette.text.secondary,
        paddingRight: '7vw',
    },
    topDiv: {
        marginTop: '3vh',
        width: '80vw',
    },
    ImgCampanha: {
        height: 'inherit',
        width: '30vw',
        borderRadius: 3,
    }, leftDiv: {
        flex: 1,
        marginRight: '3vw'
    },
    contentDiv: {
        display: 'flex'
    },
    campStatusDiv: {
        flex: 1,
        marginBottom: '5vh',
        verticalAlign: 'middle'
    },
    titleStyle: {
        paddingBottom: '2vh',
        marginBottom: '3vh'
    },
    linearBar: {
        width: '25vw',
        height: '1rem',
        display: 'inline-block',
        borderRadius: 20,

    }, buttonsDiv: {
        position: 'relative',
        textAlign: 'right'
    },
    linkStyle: {
        textDecoration: 'none',
        '&:visited, &:link, &:active': {
            textDecoration: 'none',
            color: 'inherit',
        }
    },

}));



function CampanhaItemView(props) {

    /** -------------------------------Hooks --------------------------------------------- */
    const classes = useStyles()
    const id_campanha = sessionStorage.getItem('id_camp') || null;
    const [campanha, setCampanha] = useState();
    const [loading, setLoading] = useState(true);
    const [snackbarMessage, setSnackbarMessage] = useState('')
    const [open, setOpen] = useState(false);
    const [isParticipanting, setIsParticipating] = useState(false);
    const id = getCurrentUser().id_utilizador
    const token = getToken()
    const [arraylength, setArraylength] = useState()
    const [openOwner, setOpenOwner] = useState()
    const [lastPart, setLastPart] = useState()
    const [owner, setOwner] = useState()
    let isMine;

    const getUserCamps = async () => {

        await axios.get(apiURL + '/campanhaUtilizador/' + id_campanha)
            .then((res) => {
                if (res.data.some(ele => ele.id_utilizador == id)) {
                    setIsParticipating(true)
                }
                setArraylength(res.data.length)
                setLastPart(res.data.pop())
            })
            .catch((error) => {
                setLastPart(null)
            })
            .then(() => {
                setLoading(false)
            })
    }

    const getUserNames = () => {
        axios.get(apiURL + '/utilizadores/' + owner, {
            headers: {
                authorization: 'Bearer ' + token
            }
        }).then((res) => {
            setOwner(res.data.nome)
            setOpenOwner(true)
        }).catch((error) => {
            console.log("erro uts > ", error)
        })
    }

    useEffect(() => {
        axios.get(apiURL + '/campanha/' + id_campanha)
            .then((res) => {
                setCampanha(res.data);
                setOwner(res.data.id_utilizador)
                isMine = (id == res.data.id_utilizador)
                getUserCamps()
            })
            .catch((error) => {

            })
    }, [id_campanha])


    /**----------------------------------------- Snackbar --------------------------------------------- */

    const handleClose = () => {
        setOpen(false)
    }

    const participar = () => {
        axios.post(apiURL + '/campanhaUtilizador', {
            id_campanha: id_campanha,
            id_utilizador: getCurrentUser().id_utilizador,
            data_adesao: new Date()
        })
            .then((res) => {
                setSnackbarMessage('Adicionado à campanha com sucesso')
                setOpen(true)
                setIsParticipating(true);
            })
            .catch((error) => {

            })
    }

    const removerParticipacao = () => {
        axios.delete(apiURL + '/campanhaUtilizador/' + id_campanha + '?idUt=' + id)
            .then((res) => {
                if (res.status == 200) {
                    setIsParticipating(false);
                    setSnackbarMessage('Saiu da campanha com sucesso.')
                    setOpen(true);
                }
            })
            .catch((error) => {
                alert(JSON.stringify(error, null, 2))
            })
    }

    /** ---------------------------------------- UI ------------------------------------------------ */
    return (
        !id_campanha ?
            <Paper>Erro ao carregar a campanha</Paper> :
            loading ? <Loading /> :
                <div>
                    <div style={divStyle}>
                        <Grid container spacing={3} className={classes.topDiv} >
                            <Grid item xs={12}>
                                <Paper className={classes.paper}>
                                    <div className={classes.contentDiv}>
                                        <div className={classes.leftDiv}>
                                            <img src={apiURL + '/campanhaImgUpload/' + campanha.img_path} alt="CamanhaImage" className={classes.ImgCampanha} />
                                        </div>
                                        <Typography component={'span'} variant={'body2'}>
                                            <ThemeProvider theme={theme}>
                                                <div className={classes.campStatusDiv}>
                                                    <h2 className={classes.titleStyle}>{campanha.nome}</h2>
                                                    <p style={{ marginBottom: '5vh' }}>{campanha.descricao}</p>
                                                    {campanha.fundos_disponiveis}€ <LinearProgress className={classes.linearBar} color="primary" variant="determinate" value={(campanha.fundos_disponiveis / campanha.fundos_objetivo) * 100} /> {campanha.fundos_objetivo} €
                                                </div>
                                                <div className={classes.buttonsDiv}>
                                                    <Button variant="outlined" color="secondary" style={{ marginRight: '1vw' }}>
                                                        {isMine ? <Link to="/minhasCampanhas" className={classes.linkStyle}>Voltar</Link> :
                                                            <Link to="/Campanhas" className={classes.linkStyle}>Voltar</Link>}
                                                    </Button>
                                                    {isParticipanting || id == campanha.id_utilizador ?
                                                        <Button variant="contained" color="primary" onClick={() => removerParticipacao()}>
                                                            Remover Participação
                                                    </Button>
                                                        : <Button variant="contained" color="primary" onClick={() => participar()} disabled={isParticipanting}>
                                                            <span style={{ color: 'white' }}>Participar</span>
                                                        </Button>}
                                                </div>
                                            </ThemeProvider>
                                        </Typography>
                                    </div>
                                </Paper>
                            </Grid>
                            <Grid container spacing={3} className={classes.topDiv} >
                                <Grid item xs={12}>
                                    <Paper className={classes.paper}>
                                        <div className={classes.contentDiv}>
                                            <Grid container direction="row" justify="space-between" alignItems="center">
                                                <ThemeProvider theme={theme}>
                                                    <Typography variant={'subtitle1'} component={'div'}>
                                                        Numero Utilizadores participantes: <b style={{ marginRight: '10px' }}>{arraylength}</b><span style={{ marginRight: '20px' }}>|</span>
                                                    </Typography>
                                                    <Typography variant={'subtitle1'} component={'div'}>
                                                        Ultímo participante aderiu em:  <b>{lastPart ? new Date(lastPart.data_adesao).toLocaleDateString() : "N/A"}</b>
                                                    </Typography>
                                                    <div style={{ textAlign: 'right', display: 'block', verticalAlign: 'middle', alignContent: ' flex-end' }}>
                                                        {openOwner ? <b>{owner}</b> : < Button color="primary" onClick={() => getUserNames()}>
                                                            Quem criou?
                                                        </Button>}
                                                    </div>
                                                </ThemeProvider>
                                            </Grid>
                                        </div>
                                    </Paper>
                                </Grid>
                            </Grid>
                        </Grid>
                        <Snackbar anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }} open={open} onClose={handleClose} message={snackbarMessage} key="camp" />
                    </div>
                </div >
    );
}

const divStyle = {
    display: 'flex',
    justifyContent: 'center'
}

export default CampanhaItemView;