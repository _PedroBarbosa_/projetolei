import React, { useEffect, useState } from 'react';
import { makeStyles, Grid } from '@material-ui/core';
import { theme } from '../../../theme';
import axios from 'axios';
import Loading from '../Loading';
import { apiURL } from '../../../AxiosConfig';
import CampanhaItem from './CampanhaItem';

const useStyles = makeStyles((theme) => ({
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
    topDiv  : {
        marginTop: '3vh',
        width: '80vw',
        alignItems: 'center',
    },
    divStyle: {
        display: 'flex',
        justifyContent: 'center'
    }
}));

function ListaCampanhas(props) {

    const classes = useStyles();
    const [data, setData] = useState();
    const [loading, setLoading] = useState(true);


    useEffect(() => {
        axios.get(apiURL + '/campanha')
            .then((res) => {
                setData(res.data[0]);
                setLoading(false);
            }).catch((error) => {
            }).then(() => {
            })
    }, [])


    return (
        loading ? <Loading /> :
            <div className={classes.divStyle}>
                <Grid container className={classes.topDiv} >
                    <Grid container justify="center" spacing={3}>
                        {data.map((values) => <CampanhaItem loading={loading} values={values} img={values.img_path} key={values.id_campanha} />)}
                    </Grid>
                </Grid>
            </div>
    );
}

export default ListaCampanhas;