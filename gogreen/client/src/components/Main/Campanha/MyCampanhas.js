import React, { useEffect, useState } from 'react';
import { makeStyles, Grid, ThemeProvider, Typography, Paper } from '@material-ui/core';
import { theme } from '../../../theme';
import axios from 'axios';
import Loading from '../Loading';
import { apiURL } from '../../../AxiosConfig';
import { getCurrentUser } from '../../../services/Utils';
import MyCampanhaItem from './MyCampanhaItem';

const useStyles = makeStyles((theme) => ({
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
    topDiv: {
        marginTop: '3vh',
        width: '80vw',
        alignItems: 'center',
        marginBottom: '2vh'
    },
    divStyle: {
        display: 'flex',
        justifyContent: 'center'
    }
}));

function MyCampanhas(props) {

    const classes = useStyles();
    const [data, setData] = useState();
    const [loading, setLoading] = useState(true);
    const [isEmpty, setIsempty] = useState(false);

    var id;
    if (getCurrentUser()) id = getCurrentUser().id_utilizador;

    useEffect(() => {
        axios.get(apiURL + '/campanhaU/' + id)
            .then((res) => {
                setData(res.data);
                if (res.data == []) setIsempty(true);
                setLoading(false);
            }).catch((error) => {
            }).then(() => {
            })
    }, [])


    return (
        loading ? <Loading /> : isEmpty ? <dvi> "Oops parece que não tem nenhuma campanha!</dvi> :
            <div className={classes.divStyle}>
                <Grid container direction="column" justify="center" alignItems="center" className={classes.topDiv}>
                    <Grid item xs={12} className={classes.topDiv}>
                        <ThemeProvider theme={theme}>
                            <Typography variant="h5" color="secondary">
                                <Paper className={classes.paper}>
                                    As minhas Campanhas
                                </Paper>
                            </Typography>
                        </ThemeProvider>
                    </Grid>
                    <Grid container justify="center" spacing={3}>
                        {data.map((values) => <MyCampanhaItem loading={loading} values={values} img={values.img_path} key={values.id_campanha} />)}
                    </Grid>
                </Grid>
            </div>
    );
}

export default MyCampanhas;