import React, { useContext } from 'react'
import background from '../../images/backgroundMain.png';
import Grid from '@material-ui/core/grid';
import { makeStyles } from '@material-ui/core/styles';
import { Typography } from '@material-ui/core';
import { theme } from '../../theme';
import { ThemeProvider } from '@material-ui/core/styles';
import Paper from '@material-ui/core/paper';
import { Button } from '@material-ui/core';
import { Link } from 'react-router-dom';
import HowToRegIcon from '@material-ui/icons/HowToReg';
import logo from '../../images/gogreenLogoPrimary.png'
import LogginContext from '../../context'


const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    control: {
        padding: theme.spacing(2),
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'justified',
        color: theme.palette.text.secondary,
        fontSize: '1vw',
        marginTop: '-20%'
    },
    gridTop: {
        marginTop: 30
    },
    title: {
        marginRight: '2vh',
        marginBottom: '5vw',
        color: 'white',
        fontSize: '7vw'
    },
    leftSideGrid: {
        marginLeft: '5vh',
        marginRight: '-17vh'
    },
    sloganP: {
        margin: 0,
        padding: 0,
        paddingRight: 10,
        paddingLeft: 10,
        color: 'green',
        fontSize: '3vw',
    },
    sloganB: {
        borderRadius: 10,
        paddingLeft: 10,
    },
    imgLogo: {
        backgroundColor: 'white',
        padding: '1vh',
        borderRadius: '100%',
        boxShadow: '100px 100px 1000px white',
        height: '20vw'
    },
    linkStyle: {
        textDecoration: 'none',
        '&:visited, &:link, &:active': {
            textDecoration: 'none',
            color: 'white',
        },
    }
}));

export default function Home() {

    const classes = useStyles();
    const { value } = useContext(LogginContext)

    return (
        <div style={styleHome}>
            <Grid container className={classes.root} spacing={2}>
                <Grid item xl={12}>
                    <Grid container justify="flex-end" spacing={2} className={classes.gridTop}>
                        <ThemeProvider theme={theme}>
                            <Typography color="primary" align="right" display="block" variant="h1" className={classes.title}>
                                GoGreen <img src={logo} alt="logo" style={{ height: '7vw' }}></img>
                            </Typography>
                        </ThemeProvider>
                    </Grid>
                </Grid>
            </Grid>
            <ThemeProvider theme={theme}>
                <Typography style={{ marginLeft: '2vh' }} variant="subtitle1" >
                    <Grid container direction="row" justify="space-evenly" alignItems="center" >
                        <Grid item xs={12} sm={6} className={classes.leftSideGrid}>
                            <img src={background} alt="imgLogo" className={classes.imgLogo}></img>
                        </Grid >
                        <Grid item xs={12} sm={3}>
                            <Paper className={classes.paper}>
                                <h2 style={{ marginBottom: '20px' }}>Ao reciclar está a ganhar</h2>
                                <p style={{ marginBottom: 10 }}>Recicle normalmente mas em vez de depositar nos contentores habituais, marque uma recolha com os nossos serviços e ganhe pontos!</p>
                                <p><b>Registe-se</b> já na plataforma e comece a ganhar pontos!</p>
                                <div style={{ marginTop: 20, textAlign: "right", marginRight: 20, verticalAlign: 'center' }}>
                                    <Button disableElevation variant="contained" disabled={value} color="primary" ><Link to="/Registar" className={classes.linkStyle}>Registar<HowToRegIcon style={{ marginLeft: 15, verticalAlign: 'middle' }} /></Link></Button>
                                </div>
                            </Paper>
                        </Grid>
                    </Grid>
                </Typography>
            </ThemeProvider>

        </div >
    )
}

const styleHome = {
    //backgroundImage: 'url(' + background + ')',
    //backgroundColor: '#FFFFFF',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: '50% 50%',
    backgroundSize: 'auto',
    marginRight: '5vw',
    marginLeft: '5vw',
    height: '100%'
}