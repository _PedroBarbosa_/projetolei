import React from 'react'
import { theme } from '../../theme';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import { Grid, ThemeProvider, Typography } from '@material-ui/core';
import img from '../../images/trashcans.png'

const useStyles = makeStyles((theme) => ({
    paperMain: {
        // padding: theme.spacing(2),
        textAlign: 'justify',
        color: theme.palette.text.secondary,
        height: 'inherit',
        padding: '2vh',
        marginTop: '7vh',
        fontSize: '1vw',
        width: '35vw',
        marginLeft: '2vw'
    },
    leftGrid: {
    },
    title: {
        marginTop: '3vh',
        fontSize: '7vw'
    },
    imgStyle: {
        marginTop: '5vh',
        width: '30vw'
    }
}));

export default function Descobrir() {

    const classes = useStyles();

    return (
        <div style={styleHome}>
            <Grid container className={classes.root} spacing={2}>
                <Grid item xl={12}>
                    <Grid container direction="row" justify="space-evenly" alignItems="center" spacing={2} className={classes.gridTop}>
                        <Grid item xs={6} sm={3} className={classes.leftGrid}>
                            <ThemeProvider theme={theme}>
                                <Typography color="primary" align="right" display="block"  variant="h1" className={classes.title}>
                                    Reciclar
                                    </Typography>
                                <img src={img} alt="imagem caixotes" className={classes.imgStyle}></img>
                            </ThemeProvider>
                        </Grid>
                        <Grid item xs={6} sm={3}>
                            <Paper className={classes.paperMain} >
                                <ThemeProvider theme={theme}>
                                    <Typography color="secondary" component={'div'} align="justify" display="block" style={{fontSize: 'inherit'}}>
                                        <h2 style={{ marginBottom: '5vh' ,fontSize:'inherit'}}>Reciclar </h2>
                                        <p style={{ marginBottom: '2vh' ,fontSize:'inherit'}}>Reciclar possibilita a redução dos materiais existentes em circulação assim reduzindo a quantidade de lixo produzido pelo ser humano.</p>
                                        <b>Plásticos</b>
                                        <p style={{ marginBottom: '2vh' ,fontSize:'inherit'}}>
                                            Garrafas de água, garrafas de produtos de limpeza, yogurtes, embrulhos de manteiga, papel adesivo, pacotes de leite e garrafas de champô ou sabão.
                                        </p>
                                        <b>Metais</b>
                                        <p style={{ marginBottom: '2vh' ,fontSize:'inherit'}}>
                                           latas de sopa,latas de comida de animais domésticos, latas de sumo e latas de comida preparada.
                                        </p>
                                        <b>Papeis</b>
                                        <p style={{ marginBottom: '2vh' ,fontSize:'inherit'}}>
                                            Papel, cartolinas, caixas de cartão, caixas de ovos, rolo de papel higiénico, Tetra pack, pacotes de sumo, jornais, revistas, cartão de papel e caixas de cereais.
                                        </p>
                                        <b>Produtos Pesados</b>
                                        <p style={{ marginBottom: '2vh' ,fontSize:'inherit'}}>
                                            Torradeiras, Ferros de engomar, máquinas de café, liquidificadoras, panelas e tachos,Televisôes e Frigorificos
                                        </p>
                                    </Typography>
                                </ThemeProvider>
                            </Paper>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid >
        </div >
    )
}

const styleHome = {
    //backgroundImage: 'url(' + background + ')',
    //backgroundColor: '#FFFFFF',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: '50% 50%',
    backgroundSize: 'auto',
    marginRight: '5vw',
    marginLeft: '5vw',
    
}