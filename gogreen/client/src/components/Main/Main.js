import React, { useContext } from 'react'
import { Switch, Route, Redirect } from 'react-router-dom'
import Home from './Home';
import Descobrir from './Descobrir';
import UserDashboard from './DashBoard/UserDashboard';
import Registar from './Registar/Registar';
import ProtectedRoute from '../../ProtectedRoute';
import ContextLogin from '../../context'
import ListaCamapanhas from './Campanha/ListaCampanhas';
import CampanhaItemView from './Campanha/CampanhaItemView';
import myRecolhas from './Recolha/myRecolhas';
import MyProdRec from './ProdRec/MyProdRec';
import MyCampanhas from './Campanha/MyCampanhas';
import ProdMarket from './ProdRec/ProdMarket';
import ProdView from './ProdRec/ProdView';
import EditProd from './ProdRec/MyEditProd';
import EditProfile from './EditProfile';
import FuncionarioBoard from './Funcionario/FuncionarioBoard';


export default function Main() {

    /**Hooks */
    const { value } = useContext(ContextLogin)

    /** Conditional rendering para o redirect quando o url não se encontra no router*/
    const condrend = () => (
        value ? <Redirect to="/UserDashboard" /> : <Redirect to="/" />
    )

    return (
        <div>
            <Switch>
                <Route exact path="/" component={Home} />
                <Route exact path="/Descobrir" component={Descobrir} />
                <ProtectedRoute exact path="/UserDashboard" component={UserDashboard} />
                <Route exact path="/Registar" component={Registar} />
                <ProtectedRoute exact path="/Campanhas" component={ListaCamapanhas} /> 
                <Route exact path="/verCampanha" component={CampanhaItemView} />
                <ProtectedRoute exact path="/Recolhas" component={myRecolhas} />
                <ProtectedRoute exact path="/verMeusProdutos" component={MyProdRec} />
                <ProtectedRoute exact path="/minhasCampanhas" component={MyCampanhas} />
                <ProtectedRoute exact path="/Market" component={ProdMarket} />
                <ProtectedRoute exact path="/Produto" component={ProdView} />
                <ProtectedRoute exact path="/editProduto" component={EditProd} />
                <ProtectedRoute exact path="/editPerfil" component={EditProfile} />
                <Route exact path="/Funcionarioboard" component={FuncionarioBoard} />
                <Route path="*" component={condrend} />
            </Switch>
        </div>
    )
}