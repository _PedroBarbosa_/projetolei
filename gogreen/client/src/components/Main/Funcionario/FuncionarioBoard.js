import React, { useContext, useState, useEffect } from 'react';
import FuncContext from '../../../FuncContext';
import LoginContext from '../../../context';
import Axios from 'axios';
import { apiURL } from '../../../AxiosConfig';
import { theme } from '../../../theme'
import { Grid, ThemeProvider, Paper, Typography, makeStyles, TableContainer, Table, TableHead, TableRow, TableCell, TableBody, Button, Modal, Dialog } from '@material-ui/core';
import DoneIcon from '@material-ui/icons/Done';
import FuncRecolhasItem from './FuncRecolhasItem';

const useStyles = makeStyles((theme) => ({
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
    topDiv: {
        marginTop: '3vh',
        width: '80vw',
        alignItems: 'center'
    },
    linkStyle: {
        textDecoration: 'none',
        '&:visited, &:link': {
            textDecoration: 'none',
            color: 'inherit',
        },
        '&:active': {
            backgroundColor: 'grey'
        },
        '& :hover': {
            backgroundColor: 'lightgrey'
        }
    },
}));

function FuncionarioBoard(props) {

    const classes = useStyles();
    const { value } = useContext(LoginContext)
    const { isFunc } = useContext(FuncContext)
    const [loading, setLoading] = useState(true)
    const [recolhas, setRecolhas] = useState()
    const [open, setOpen] = useState(false);
    const [rowR, setRowR] = useState()

    const handleOpen = (row) => {
        setRowR(row)
        setOpen(true);
    }

    const handleClose = () => {
        setOpen(false);
    }


    useEffect(() => {
        Axios.get(apiURL + '/RecolhasF/' + value)
            .then((res) => {
                setRecolhas(res.data)
                setLoading(false)
            }).catch((error) => {

            })
    }, [])

    const body = <FuncRecolhasItem values={rowR} handleClose={handleClose} />

    return (
        !loading ?
            isFunc ?
                <div>
                    <Grid container direction="column" justify="center" alignItems="center">
                        <Grid item xs={12} className={classes.topDiv}>
                            <ThemeProvider theme={theme}>
                                <Paper className={classes.paper}>
                                    <Typography variant="h5" color="secondary">
                                        Recolhas Realizadas
                            </Typography>
                                </Paper>
                            </ThemeProvider>
                        </Grid>
                        <TableContainer component={Paper} className={classes.topDiv} elevation={2}>
                            <Table className={classes.table} aria-label="simple table">
                                <TableHead>
                                    <TableRow>
                                        <TableCell align="left">Data de recolha</TableCell>
                                        <TableCell align="left">Utilizador</TableCell>
                                        <TableCell align="left">Realizada</TableCell>
                                        <TableCell align="left"></TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {Object.values(recolhas).map((row, index) => (
                                        <TableRow key={index}>
                                            <TableCell align="left">{new Date(row.data).toLocaleDateString()}</TableCell>
                                            <TableCell aligm="left">{row.id_utilizador}</TableCell>
                                            <TableCell align="left">{row.realizada ? <DoneIcon /> : null}</TableCell>
                                            <TableCell align="right"><Button onClick={() => handleOpen(row)}>Ver</Button></TableCell>
                                        </TableRow>
                                    ))}
                                </TableBody>
                            </Table>
                        </TableContainer>
                    </Grid >
                    <Dialog
                        open={open}
                        onClose={handleClose}
                        aria-labelledby="simple-modal-title"
                        aria-describedby="simple-modal-description"
                        values={rowR}
                    >
                        {body}
                    </Dialog>
                </div>
                : null
            : null
    );
}

export default FuncionarioBoard;