import React, { useEffect, useState, useContext } from 'react';
import { DialogTitle, DialogActions, Button, DialogContent, CircularProgress, makeStyles, TableHead, TableCell, TableBody, TableRow, Table } from '@material-ui/core';
import Axios from 'axios';
import { apiURL, apiURL2 } from '../../../AxiosConfig';
import CloseIcon from '@material-ui/icons/Close'
import LoginContext from '../../../context'

const useStyles = makeStyles((theme) => ({
    closeButton: {
        float: 'right',
        '&:hover': {
            background: 'lightgray',
            borderRadius: '100%'
        }
    },
    itemStyle: {
        border: '1px solid lightgray',
        padding: 5,
        borderRadius: 10,
        marginRight: 20,
    },
    lineTable: {
        marginBottom: 10,
    },
    table: {
        marginBottom: 20
    },
    actionButtons: {
        margin: 10,
        backgroundColor: 'forestgreen',   
    }
}))

const acceptRecolha = (id, idUt, funcid) => {
    Axios.put(apiURL + '/recolhasDone/' + id)
        .then((res) => {
            if (res.status == 200) alert("all gud")
            setTimeout(() => deleteMsgsFunc(idUt, funcid), 100)
        }).catch((error) => {
            console.log(error)
        })
}

const deleteMsgsFunc = (idUt, funcid) => {
    Axios.delete(apiURL2 + '/funcionariosDMs/' + idUt + '?func=' + funcid)
        .then((res) => {

        }).catch((error) => {
            console.log(error)
        })
}

function FuncRecolhasItem(props) {

    const classes = useStyles();
    const { value } = useContext(LoginContext)
    const [loading, setLoading] = useState(true)
    const [materiais, setMateriais] = useState()

    useEffect(() => {
        Axios.get(apiURL + '/recolhasMat/' + props.values.id_recolha)
            .then((res) => {
                setMateriais(res.data)
                setLoading(false)
            }).catch((error) => {

            })
    }, [])

    return (
        loading ? <CircularProgress /> :
            <div className={classes.diag}>
                <div className={classes.closeButton}><CloseIcon onClick={props.handleClose} /> </div>
                <DialogTitle id="recolha">Recolha nº {props.values.id_recolha}</DialogTitle>
                <DialogContent>
                    <Table className={classes.table}>
                        <TableHead>
                            <TableRow>
                                <TableCell align="left">Residuo</TableCell>
                                <TableCell align="left">Quantidade</TableCell>
                                <TableCell align="left">Categoria</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {materiais.map((element) => <TableRow key={element.line}>
                                <TableCell align="left">{element.Nome_residuo}</TableCell>
                                <TableCell align="left">{element.quantidade}</TableCell>
                                <TableCell align="left">{element.cat}</TableCell>
                            </TableRow>)}
                        </TableBody>
                    </Table>
                </DialogContent>
                <DialogActions>
                    {props.values.realizada ? null : <div>
                        <Button autoFocus variant="outlined" onClick={props.handleClose}>
                            Cancelar
                    </Button>
                        <Button type="submit" variant="contained" color="primary" className={classes.actionButtons}onClick={() => acceptRecolha(props.values.id_recolha, props.values.id_utilizador, value)}>
                            Confirmar
                    </Button></div>}
                </DialogActions>
            </div>
    );
}

export default FuncRecolhasItem;