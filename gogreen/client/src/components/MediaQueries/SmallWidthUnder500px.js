import useMediaQuery from '@material-ui/core/useMediaQuery';

const SmallWidth_under500px = () => {

    const is500 = useMediaQuery('(max-width:500px)');
    
    return is500;
};

export default SmallWidth_under500px;