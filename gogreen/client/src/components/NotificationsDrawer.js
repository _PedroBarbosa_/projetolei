import React, { useState, useEffect, useContext } from 'react';
import { Typography, makeStyles, Button, CircularProgress } from '@material-ui/core';
import LoginContext from "../context";
import { apiURL } from '../AxiosConfig';
import NotificationItem from './Main/NotificationItem';
import Axios from 'axios';

const useStyles = makeStyles(() => ({
    titleDiv:
    {
        color: 'whitesmoke',
        textAlign: 'center',
        verticalAlign: 'middle',
        marginBottom: '2vh',
        marginLeft: '1vw',
    },
    clearButton: {
        color: 'white',
        border: '1px solid whitesmoke'
    },
}));

function NotificationsDrawer(props) {

    const classes = useStyles();
    const [notifications, setNotifications] = useState();
    const [loading, setLoading] = useState(true);
    const { value } = useContext(LoginContext);

    useEffect(() => {
        Axios.get(apiURL + '/utilizadorNotificacoes/' + value)
            .then((res) => {
                setNotifications(res.data)
                setLoading(false)
            }).catch((error) => {
                console.log(error)
                setLoading(false)
            })
    }, [])

    const clearNotifications = () => {
        setNotifications([])
        Axios.delete(apiURL + '/utilizadorNotificacoes/' + value)
        .then((res) => {

        }).catch((error) => {
            console.log(error)
        })
    }

    return (
        <div>
            <div style={{ marginTop: '2vh' }}>
                <Typography variant={'h6'} component={'span'} align="center" className={classes.titleDiv}>
                    Notificações
                </Typography>
            </div>
            <div style={{ marginTop: '1vh', textAlign: 'right', marginBottom: '2vh' }}>
                <Button variant="contained" classes={{ outlined: classes.clearButton }}
                    onClick={() =>  clearNotifications()}>
                    Limpar
                </Button>
            </div>
            {loading ? <CircularProgress /> : <div>
                {notifications ?
                    notifications.map((values, index) => <NotificationItem values={values} key={index} />)
                    : <span className={classes.titleDiv}>Sem notificações</span>}
            </div>}
        </div>
    );
}

export default NotificationsDrawer;