import React from 'react'

const FuncContext = React.createContext({isFunc: false});

export default FuncContext;