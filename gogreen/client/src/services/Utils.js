
export const logout = () => {
    localStorage.clear();
    sessionStorage.clear();
}

export const getCurrentUser = () => {
    return JSON.parse(localStorage.getItem('utilizador'));
}

export const getToken = () => {
    return JSON.parse(localStorage.getItem('token'));
  }

