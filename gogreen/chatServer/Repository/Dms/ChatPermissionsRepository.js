var response = require('../../Database/response');
var TYPES = require('tedious').TYPES;

function ChatPermissionsRepository(dbContext) {

    function getFuncionariosDMs(req, res, next) {
        
        if (req.params.id) {
            var parameters = [];

            parameters.push({ name: 'id_utilizador', type: TYPES.Int, val: req.params.id });

            var query = "select * from Func_DMs where id_utilizador = " + req.params.id

            dbContext.getQuery(query, parameters, true, function (error, data) {
                if (data) {
                    req.data = data[0];
                    return next();
                }
                return res.sendStatus(404);
            });
        }
    }

    function getDM(req, res) {
        return res.json(req.data);
    }

    function postChatPermition(req, res) {

        var parameters = [];

        parameters.push({ name: 'id_utilizador', type: TYPES.Int, val: req.body.id_utilizador });
        parameters.push({ name: 'id_funcionario', type: TYPES.Int, val: req.body.id_funcionario });

        dbContext.post("AdicionarPermissaoChat", parameters, function (error, data) {
            return res.json(response(data, error));
        });
    }

    function deleteConversation(req, res) {

        var parameters = [];

        if (req.params.id && req.query.func) {
            var parameters = [];

            parameters.push({ name: 'id_funcionario', type: TYPES.Int, val: req.params.id });

            var query = "delete from Func_Dms where id_utilizador = " + req.params.id + " AND id_funcionario = " +req.query.func

            dbContext.getQuery(query, parameters, false, function (error, data, rowCount) {
                if (rowCount > 0) {
                    return res.json('Record is deleted');
                }
                return res.sendStatus(404);
            });
        }
    }




    return {
        intercept: getFuncionariosDMs,
        get: getDM,
        post: postChatPermition,
        delete: deleteConversation
    }
}

module.exports = ChatPermissionsRepository;