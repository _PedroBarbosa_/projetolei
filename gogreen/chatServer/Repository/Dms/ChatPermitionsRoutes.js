const _chatPermitionsRepository = require('./ChatPermissionsRepository');
const dbContext = require('../../Database/dbContext');

module.exports = function (router) {
    const chatpermRepository = _chatPermitionsRepository(dbContext);

    router.route('/funcionariosDMs')
        //.get(chatpermRepository.getAll)
        .post(chatpermRepository.post);

    router.use('/funcionariosDMs/:id', chatpermRepository.intercept);

    router.route('/funcionariosDMs/:id')
        .get(chatpermRepository.get)
        .delete(chatpermRepository.delete);
}