const _chat = require('./ChatRepository');
const dbContext = require('../../Database/dbContext');

module.exports = function (router) {
    const chat = _chat(dbContext)

    router.route('/unreadMessages')
        .put(chat.put)
        .post(chat.post);

    router.use('/unreadMessages/:id/:id2', chat.getUnread);

    router.use('/getMsgs/:id/:id2', chat.getMessages)

  //  router.route('/getMsgs/:id/:id2')
    //    .get(chat.get)

}