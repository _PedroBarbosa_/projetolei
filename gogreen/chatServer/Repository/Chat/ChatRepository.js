var response = require('../../Database/response');
var TYPES = require('tedious').TYPES;

function ChatRepository(dbContext) {

    function getUserMessages(req, res, next) {

        if (req.params.id && req.params.id2) {
            var parameters = [];

            parameters.push({ name: 'id_remetente', type: TYPES.Int, val: req.params.id });
            parameters.push({ name: 'id_destino', type: TYPES.Int, val: req.params.id2 });

            var query = "select * from Dms where id_remetente = " + req.params.id + " AND id_destino = " + req.params.id2
                + " OR id_remetente = " + req.params.id2 + " AND id_destino = " + req.params.id
                + " ORDER BY data ASC"



            dbContext.getQuery(query, parameters, true, function (error, data) {
                if (data) {
                    req.data = data[0];
                    return res.json(data[0])
                }

            });
        }
    }

    function getUserUnreadMessages(req, res, next) {

        
        if (req.params.id && req.params.id2) {
            var parameters = [];

            parameters.push({ name: 'id_remetente', type: TYPES.Int, val: req.params.id})
            parameters.push({ name: 'id_destino', type: TYPES.Int, val: req.params.id2 })

            dbContext.post("getUserUnreadMessages", parameters, function (error, data) {
                
                return res.json(response(data, error))
            })
        }
    }

    function getMessage(req, res) {
        return res.json(req.data)
    }

    function readAllMessages(req, res) {

        var parameters = []

        parameters.push({ name: 'id_remetente', type: TYPES.Int, val: req.body.id_remetente })
        parameters.push({ name: 'id_destino', type: TYPES.Int, val: req.body.id_destino })


        dbContext.post("readAllMessages", parameters, function (error, data) {
            return res.json(response(data, error))
        })
    }

    function addMessage(req, res) {

        parameters = []

        parameters.push({ name: 'id_remetente', type: TYPES.Int, val: req.body.id_remetente });
        parameters.push({ name: 'id_destino', type: TYPES.Int, val: req.body.id_destino });
        parameters.push({ name: 'mensagem', type: TYPES.VarChar, val: req.body.mensagem });


        dbContext.post("InserirDm", parameters, function (error, data) {
            return res.json(response(data, error))
        })

    }


    return {
        get: getMessage,
        getUnread: getUserUnreadMessages,
        getMessages: getUserMessages,
        post: addMessage,
        put: readAllMessages
    }
}

module.exports = ChatRepository;