const express = require('express');
const { Router } = require('express');

function eRoutes() {
    const router = express.Router();
    var chat = require('./Repository/Chat/ChatRoutes')(router);
    require('./Repository/Dms/ChatPermitionsRoutes')(router);
    return router
}

module.exports = eRoutes;