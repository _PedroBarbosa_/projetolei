const express = require('express');
const app = express();
var bodyParser = require('body-parser');
var cors = require('cors');
var routes = require('./router')();

const port = process.env.port || 3301

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(cors());

app.listen(port, () => {
    console.log("Servidor chat encontra-se ligado na porta " + port)
})

app.use('/api', routes);