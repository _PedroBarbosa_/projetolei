var bcrypt = require('bcrypt');
var utils = require('../../shared/utils');
var jwt = require('jsonwebtoken')
var crypto = require('crypto');


function LoginRepository(dbContext) {

    /**Autenticar o utilizador */
    function checkLogin(req, res, next) {

        var parameters = [];

        if (req.body.email && req.body.password) {

            var query = "Select * from Utilizador WHERE email = '" + req.body.email + "'";

            dbContext.getQuery(query, parameters, false, function (error, data) {
                //Utilizador
                if (data) {
                    req.data = data[0];

                    if (bcrypt.compareSync(req.body.password, req.data.password)) {
                        return next();
                    }
                }
                //Funcionario
                query = "Select * from FuncLogin WHERE username = '" + req.body.email + "'"
                    + " AND password = '" + req.body.password + "'";

                dbContext.getQuery(query, parameters, false, function (error, data) {
                    console.log("data func > ", data)
                    if (data) {
                        req.data = data[0];
                        const token = utils.generateToken(req.data);
                        const func = utils.getCleanFunc(data[0])
                        return res.json({ funcionario: func, token })
                    } else {
                        return res.sendStatus(404)
                    }
                })
            })
    }
}

function getUtilizador(req, res,) {
    const token = utils.generateToken(req.data);
    const user = utils.getCleanUser(req.data);
    return res.json({ utilizador: user, token });
}

function verifyToken(req, res) {

    const authHeader = req.headers['authorization'];
    var token = authHeader && authHeader.split(' ')[1]

    if (token) {
        jwt.verify(token, process.env.JWT_SECRET, (err, user) => {
            if (err) {
                return res.sendStatus(403);
            }
            return res.json({ id: user.id_utilizador })
        })
    }
}

return {
    post: getUtilizador,
    login: checkLogin,
    verify: verifyToken
}
}

module.exports = LoginRepository;