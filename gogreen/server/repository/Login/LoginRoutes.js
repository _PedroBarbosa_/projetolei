const _loginRepository = require('./LoginRepository');
const dbContext = require('../../Database/dbContext');

module.exports = function (router) {
    const loginRepository = _loginRepository(dbContext);

    router.use('/login', loginRepository.login);

    router.route('/login')
        .post(loginRepository.post);

    router.route('/verify')
        .get(loginRepository.verify);

}