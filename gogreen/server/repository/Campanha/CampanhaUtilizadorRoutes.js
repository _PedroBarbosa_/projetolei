const _campanhaUtilizadorRepository = require('./CampanhaUtilizadorRepository');
const dbContext = require('../../Database/dbContext');

module.exports = function (router) {
    const campanhaUtlizadorRepository = _campanhaUtilizadorRepository(dbContext);

    router.route('/campanhaUtilizador')
        .get(campanhaUtlizadorRepository.getAll)
        .post(campanhaUtlizadorRepository.post);

    router.use('/campanhaUtilizador/:id', campanhaUtlizadorRepository.intercept);

    router.route('/campanhaUtilizador/:id')
        .get(campanhaUtlizadorRepository.get)
       // .put(campanhaUtlizadorRepository.put)
        .delete(campanhaUtlizadorRepository.delete);
}