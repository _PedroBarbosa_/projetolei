const _campanhaRepository = require('./CampanhaRepository');
const dbContext = require('../../Database/dbContext');

module.exports = function (router) {
    const campanhaRepository = _campanhaRepository(dbContext);

    router.route('/campanha')
        .get(campanhaRepository.getAll)
        .post(campanhaRepository.post);

    router.use('/campanha/:id', campanhaRepository.intercept);

    router.route('/campanhaImgUpload')
        .post(campanhaRepository.upload);

    router.route('/campanhaImgUpload/:filename')
        .get(campanhaRepository.getImg);

    router.route('/campanha/:id')
        .get(campanhaRepository.get)
        .put(campanhaRepository.put)
        .delete(campanhaRepository.delete);

    router.use('/campanhaU/:id', campanhaRepository.interceptU);

    router.route('/campanhaU/:id')
        .get(campanhaRepository.get);
}