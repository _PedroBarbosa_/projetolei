var response = require('../../shared/response');
var TYPES = require('tedious').TYPES;
const fileUpload = require('express-fileupload');
const fs = require('fs');
const path = require('path');

function CampanhaRepository(dbContext) {

    function findCampanha(req, res, next) {

        if (req.params.id) {
            var parameters = [];

            parameters.push({ name: 'id_campanha', type: TYPES.Int, val: req.params.id });

            var query = "select * from Campanha where id_campanha = " + req.params.id

            dbContext.getQuery(query, parameters, false, function (error, data) {
                if (data) {
                    req.data = data[0];
                    return next();
                }
                return res.sendStatus(404);
            });
        }
    }

    function getCampanhas(req, res) {

        dbContext.get("getCampanhas", function (error, data) {
            return res.json(response(data, error));
        });
    }

    function getCampanha(req, res) {
        return res.json(req.data);
    }

    function postCampanha(req, res) {

        var parameters = [];

        parameters.push({ name: 'nome', type: TYPES.VarChar, val: req.body.nome });
        parameters.push({ name: 'descricao', type: TYPES.VarChar, val: req.body.descricao });
        parameters.push({ name: 'fundos_disponiveis', type: TYPES.Float, val: req.body.fundos_disponiveis });
        parameters.push({ name: 'fundos_objetivo', type: TYPES.Float, val: req.body.fundos_objetivo });
        parameters.push({ name: 'id_utilizador', type: TYPES.Int, val: req.body.id_utilizador });
        parameters.push({ name: 'img_path', type: TYPES.VarChar, val: req.body.img_path });

        dbContext.post("InserirAlterarCampanha", parameters, function (error, data) {
            return res.json(response(data, error));
        });
    }

    function putCampanha(req, res) {

        var parameters = [];

        Object.entries(req.data).forEach((property) => {

            if (req.body[property[0]]) {
                parameters.push(
                    {
                        name: property[0],
                        val: req.body[property[0]],
                        type: TYPES.VarChar
                    });

            } else {
                parameters.push(
                    {
                        name: property[0],
                        val: property[1],
                        type: TYPES.VarChar
                    });
            }
        });

        dbContext.post("InserirAlterarCampanha", parameters, function (error, data) {
            return res.json(response(data, error));
        });
    }

    function deleteCampanha(req, res) {

        var parameters = [];
        console.log("teste");
        if (req.params.id) {
            var parameters = [];

            parameters.push({ name: 'id_campanha', type: TYPES.Int, val: req.params.id });

            var query = "delete from Campanha where id_campanha = " + req.params.id;

            dbContext.getQuery(query, parameters, false, function (error, data, rowCount) {
                if (rowCount > 0) {
                    return res.json('Record is deleted');
                }
                return res.sendStatus(404);
            });
        }
    }

    function Upload(req, res) {

        if (req.files == null) return res.status(400).json({ mensagem: 'Nenhum ficheiro carregado' });

        const file = req.files.file;

        file.mv(`images/${file.name}`, error => {
            if (error) {
                console.log(error)
                return res.status(500).json({ erro: error });
            }

            return res.json({ fileName: file.name, filePath: 'images/' + file.name })
        })
    }

    function getImg(req, res) {

        const filename = req.params.filename;
        const pathFile = path.join(__dirname,'../../../server/images/',filename)

        fs.readFile(pathFile, (error, data) => {
            if (error) {
                console.log(error);
                return res.sendStatus(500);
            }

            if (!data) return res.status(404).json({ erro: "Ficheiro nao existente" });

            return res.sendFile(pathFile);
        })

    }
    /**function SearchUtilizador(req, res) {

        var parameters = [];

        parameters.push({ name: 'Salary', type: TYPES.Int, val: req.query.salary });

        var query = "select * from tbl_employee where salary>=@Salary"

        dbContext.get(query, parameters, function (error, data) {
            return res.json(response(data, error));
        });
    }*/

    function findCampanhaU(req, res, next) {

        if (req.params.id) {
            var parameters = [];

            parameters.push({ name: 'id_utilizador', type: TYPES.Int, val: req.params.id });

            var query = "select * from Campanha where id_utilizador = " + req.params.id

            dbContext.getQuery(query, parameters, true, function (error, data) {
                if (data) {
                    req.data = data[0];
                    return next();
                }
                return res.sendStatus(404);
            });
        }
    }

    return {
        getAll: getCampanhas,
        get: getCampanha,
        post: postCampanha,
        put: putCampanha,
        // find: SearchUtilizador,
        intercept: findCampanha,
        delete: deleteCampanha,
        upload: Upload,
        getImg: getImg,
        interceptU: findCampanhaU,
    }
}

module.exports = CampanhaRepository;