var response = require('../../shared/response');
var TYPES = require('tedious').TYPES;

function CampanhaUtilizadorRepository(dbContext) {

    function findUtilizadoresCampanha(req, res, next) {

        if (req.params.id) {
            var parameters = [];

            parameters.push({ name: 'id_campanha', type: TYPES.Int, val: req.params.id });

            var query = "select * from Campanha_utilizadores where id_campanha = " + req.params.id

            dbContext.getQuery(query, parameters, true, function (error, data) {
                if (data) {
                    req.data = data[0];
                    return next();
                }
                return res.sendStatus(404);
            });
        }
    }

    function getCampanhas(req, res) {

        dbContext.get("getCampanhaUtilizadores", function (error, data) {
            return res.json(response(data, error));
        });
    }

    function getCampanhaUtilizadores(req, res) {
        return res.json(req.data);
    }

    function postCampanhaUtilizador(req, res) {

        var parameters = [];

        parameters.push({ name: 'id_campanha', type: TYPES.VarChar, val: req.body.id_campanha });
        parameters.push({ name: 'id_utilizador', type: TYPES.Int, val: req.body.id_utilizador });
        parameters.push({ name: 'data_adesao', type: TYPES.Date, val: req.body.data_adesao });

        dbContext.post("AdicionarUtilizadorCampanha", parameters, function (error, data) {
            return res.json(response(data, error));
        });
    }

    /*function putCampanha(req, res) {

        var parameters = [];

        Object.entries(req.data).forEach((property) => {

            if (req.body[property[0]]) {
                parameters.push(
                    {
                        name: property[0],
                        val: req.body[property[0]],
                        type: TYPES.VarChar
                    });

            } else {
                parameters.push(
                    {
                        name: property[0],
                        val: property[1],
                        type: TYPES.VarChar
                    });
            }
        });

        dbContext.post("InserirAlterarCampanha", parameters, function (error, data) {
            return res.json(response(data, error));
        });
    }*/

    function deleteCampanhaUtilizador(req, res) {

        var parameters = [];

        
        if (req.params.id && req.query.idUt) {
            var parameters = [];

            parameters.push({ name: 'id_campanha', type: TYPES.Int, val: req.params.id });
            parameters.push({ name: 'id_utilizador', type: TYPES.Int, val: req.params.idUt });

            var query = "delete from Campanha_utilizadores where id_campanha = " + req.params.id +
                " AND id_utilizador = " + req.query.idUt;
            
            dbContext.getQuery(query, parameters, false, function (error, data, rowCount) {
                if (rowCount > 0) {
                    return res.json('Record is deleted');
                }
                return res.sendStatus(404);
            });
        }
    }

    /**function SearchUtilizador(req, res) {

        var parameters = [];

        parameters.push({ name: 'Salary', type: TYPES.Int, val: req.query.salary });

        var query = "select * from tbl_employee where salary>=@Salary"

        dbContext.get(query, parameters, function (error, data) {
            return res.json(response(data, error));
        });
    }*/

    return {
        getAll: getCampanhas,
        get: getCampanhaUtilizadores,
        post: postCampanhaUtilizador,
        //put: putCampanha,
        // find: SearchUtilizador,
        intercept: findUtilizadoresCampanha,
        delete: deleteCampanhaUtilizador
    }
}

module.exports = CampanhaUtilizadorRepository;