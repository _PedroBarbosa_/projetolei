const _funcionarioRepository = require('./FuncionarioRepository');
const dbContext = require('../../Database/dbContext');

module.exports = function (router) {
    const funcionarioRepository = _funcionarioRepository(dbContext);

    router.route('/funcionarios')
        .get(funcionarioRepository.getAll)
        .post(funcionarioRepository.post);

    router.use('/funcionarios/:funcionarioId', funcionarioRepository.intercept);

    router.route('/funcionarios/:funcionarioId')
        .get(funcionarioRepository.get)
        .put(funcionarioRepository.put)
        .delete(funcionarioRepository.delete);

        router.route('/funcionariosDist/:dist')
        .get(funcionarioRepository.getFuncByDist);
}