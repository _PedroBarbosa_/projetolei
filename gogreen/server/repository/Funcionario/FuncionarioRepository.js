var response = require('../../shared/response');
var TYPES = require('tedious').TYPES;

function FuncionarioRepository(dbContext) {

    function findFuncionario(req, res, next) {

        if (req.params.funcionarioId) {
            var parameters = [];

            parameters.push({ name: 'id_funcionario', type: TYPES.Int, val: req.params.funcionarioId });

            var query = "select * from funcionario where id_funcionario = " + req.params.funcionarioId

            dbContext.getQuery(query, parameters, false, function (error, data) {
                if (data) {
                    req.data = data[0];
                    return next();
                }
                return res.sendStatus(404);
            });
        }
    }

    function getFuncionarios(req, res) {

        dbContext.get("getFuncionarios", function (error, data) {
            return res.json(response(data, error));
        });
    }

    function getFuncionario(req, res) {
        return res.json(req.data);
    }

    function postFuncionario(req, res) {

        var parameters = [];


        parameters.push({ name: 'nomeFuncionario', type: TYPES.VarChar, val: req.body.nomeFuncionario });
        parameters.push({ name: 'data_contrato', type: TYPES.Date, val: req.body.data_contrato });
        parameters.push({ name: 'salario', type: TYPES.Float, val: req.body.salario });
        parameters.push({ name: 'dept_no', type: TYPES.Int, val: req.body.dept_no });
        parameters.push({ name: 'distrito', type: TYPES.VarChar, val: req.body.distrito });


        dbContext.post("InserirAlterarFuncionario", parameters, function (error, data) {
            return res.json(response(data, error));
        });
    }

    function putFuncionario(req, res) {

        var parameters = [];

        Object.entries(req.data).forEach((property) => {

            if (req.body[property[0]]) {
                parameters.push(
                    {
                        name: property[0],
                        val: req.body[property[0]],
                        type: TYPES.VarChar
                    });
            } else {

                parameters.push(
                    {
                        name: property[0],
                        val: property[1],
                        type: TYPES.VarChar
                    });
            }
        });

        dbContext.post("InserirAlterarFuncionario", parameters, function (error, data) {
            return res.json(response(data, error));
        });
    }

    function deleteFuncionario(req, res) {

        var parameters = [];

        if (req.params.funcionarioId) {
            var parameters = [];

            parameters.push({ name: 'id_funcionario', type: TYPES.Int, val: req.params.funcionarioId });

            var query = "delete from Funcionario where id_funcionario = " + req.params.funcionarioId

            dbContext.getQuery(query, parameters, false, function (error, data, rowCount) {
                if (rowCount > 0) {
                    return res.json('Record is deleted');
                }
                return res.sendStatus(404);
            });
        }
    }

    function getFuncionarioByDist(req, res, next) {
        var parameters = [];

        if (req.params.dist) {
            parameters.push({ name: 'distrito', type: TYPES.VarChar, val: req.params.dist });

            console.log(req.params.dist)
            var query = "SELECT TOP 1 id_funcionario FROM Funcionario WHERE distrito = " + "'" + req.params.dist + "'" + " ORDER BY NEWID()"
            console.log("query > ", query)
            dbContext.getQuery(query, parameters, false, function (error, data) {
                if (data) {
                    return res.status(200).json(data[0].id_funcionario);
                }
                return res.sendStatus(400);
            });
        }

    }

    /**  function getEmployeesWothDepartment(req, res) {
  
          dbContext.get("GetEmployeeWithDepartment", function (error, data) {
              return res.json(response(data, error));
          });
      }*/

    /**function SearchFuncionario(req, res) {

        var parameters = [];

        parameters.push({ name: 'Salary', type: TYPES.Int, val: req.query.salary });

        var query = "select * from tbl_employee where salary>=@Salary"

        dbContext.get(query, parameters, function (error, data) {
            return res.json(response(data, error));
        });
    }*/

    return {
        getAll: getFuncionarios,
        get: getFuncionario,
        post: postFuncionario,
        put: putFuncionario,
        // find: SearchFuncionario,
        intercept: findFuncionario,
        delete: deleteFuncionario,
        getFuncByDist: getFuncionarioByDist
    }
}

module.exports = FuncionarioRepository;