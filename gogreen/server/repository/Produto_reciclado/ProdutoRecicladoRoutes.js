const _produtoReciclado = require('./ProdutoRecicladoRepository');
const dbContext = require('../../Database/dbContext');

module.exports = function (router) {
    const produtoReciclado = _produtoReciclado(dbContext);

    router.route('/produtoreciclado')
        .get(produtoReciclado.getAll)
        .post(produtoReciclado.post);

    router.use('/produtoreciclado/:id', produtoReciclado.intercept);

    router.route('/produtoreciclado/:id')
        .get(produtoReciclado.get)
        .put(produtoReciclado.put)
        .delete(produtoReciclado.delete);

    router.use('/produtorecicladoU/:id',produtoReciclado.interceptU);

    router.route('/produtorecicladoU/:id')
        .get(produtoReciclado.get);
}