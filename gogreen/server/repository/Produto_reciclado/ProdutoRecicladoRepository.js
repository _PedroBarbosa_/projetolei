var response = require('../../shared/response');
var TYPES = require('tedious').TYPES;

function ProdutoRecicladoRepository(dbContext) {

    function findProdutoReciclado(req, res, next) {

        if (req.params.id) {
            var parameters = [];

            parameters.push({ name: 'id_produto_rec', type: TYPES.Int, val: req.params.id });

            var query = "select * from Produto_reciclado where id_produto_rec = " + req.params.id

            dbContext.getQuery(query, parameters, false, function (error, data) {
                if (data) {
                    req.data = data[0];
                    return next();
                }
                return res.sendStatus(404);
            });
        }
    }

    function getProdutosReciclados(req, res) {

        dbContext.get("getProdutosReciclados", function (error, data) {
            return res.json(response(data, error));
        });
    }

    function getProdutoReciclado(req, res) {
        return res.json(req.data);
    }


    function postProdutoReciclado(req, res) {

        var parameters = [];

        parameters.push({ name: 'nome', type: TYPES.VarChar, val: req.body.nome });
        parameters.push({ name: 'descricao', type: TYPES.VarChar, val: req.body.descricao });
        parameters.push({ name: 'preco', type: TYPES.Float, val: req.body.preco });
        parameters.push({ name: 'img_path', type: TYPES.VarChar, val: req.body.img_path });
        parameters.push({ name: 'data', type: TYPES.Date, val: req.body.data });
        parameters.push({ name: 'id_utilizador', type: TYPES.Int, val: req.body.id_utilizador})
    
        dbContext.post("AdicionarProdutoReciclado", parameters, function (error, data) {
            return res.json(response(data, error));
        });
    }

    function putProdutoReciclado(req, res) {

        var parameters = [];

        Object.entries(req.data).forEach((property) => {

            if (req.body[property[0]]) {
                parameters.push(
                    {
                        name: property[0],
                        val: req.body[property[0]],
                        type: TYPES.VarChar
                    });
            } else {
                if (property[0] === 'data') {
                    parameters.push(
                        {
                            name: property[0],
                            val: property[1],
                            type: TYPES.Date
                        });
                } else {
                    parameters.push(
                        {
                            name: property[0],
                            val: property[1],
                            type: TYPES.VarChar
                        });
                }
            }
        });
        console.log(parameters)
        dbContext.post("AdicionarProdutoReciclado", parameters, function (error, data) {
            return res.json(response(data, error));
        });
    }

    function deleteProdutoReciclado(req, res) {

        var parameters = [];

        if (req.params.id) {
            var parameters = [];

            parameters.push({ name: 'id_produto_rec', type: TYPES.Int, val: req.params.id });

            var query = "delete from produto_reciclado where id_produto_rec = " + req.params.id;

            dbContext.getQuery(query, parameters, false, function (error, data, rowCount) {
                if (rowCount > 0) {
                    return res.json('Record is deleted');
                }
                return res.sendStatus(404);
            });
        }
    }

    /**function SearchUtilizador(req, res) {

        var parameters = [];

        parameters.push({ name: 'Salary', type: TYPES.Int, val: req.query.salary });

        var query = "select * from tbl_employee where salary>=@Salary"

        dbContext.get(query, parameters, function (error, data) {
            return res.json(response(data, error));
        });
    }*/

    function findProdutoRecicladoU(req, res, next) {

        if (req.params.id) {
            var parameters = [];

            parameters.push({ name: 'id_utilizador', type: TYPES.Int, val: req.params.id });

            var query = "select * from produto_reciclado where id_utilizador = " + req.params.id

            dbContext.getQuery(query, parameters, true, function (error, data) {
                if (data) {
                    req.data = data[0];
                    return next();
                }
                return res.sendStatus(404);
            });
        }
    }

    return {
        getAll: getProdutosReciclados,
        get: getProdutoReciclado,
        post: postProdutoReciclado,
        put: putProdutoReciclado,
        // find: SearchUtilizador,
        intercept: findProdutoReciclado,
        delete: deleteProdutoReciclado,
        interceptU: findProdutoRecicladoU
    }
}

module.exports = ProdutoRecicladoRepository;