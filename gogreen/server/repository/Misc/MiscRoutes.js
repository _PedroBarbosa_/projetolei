const _miscRepository = require('./MiscRepository');
const dbContext = require('../../Database/dbContext');

module.exports = function (router) {
    const miscRepository = _miscRepository(dbContext);

    router.route('/materiais')
        .get(miscRepository.getAll)

    router.use('/materias', miscRepository.get);

    router.route('/categorias')
        .get(miscRepository.getC)
}