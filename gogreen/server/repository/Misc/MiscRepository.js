var response = require('../../shared/response');
var TYPES = require('tedious').TYPES;

function MiscRepository(dbContext) {

    function getMateriais(req, res) {

        dbContext.get("getMateriais", function (error, data) {
            return res.json(response(data, error));
        });
    }

    function getCategorias(req, res) {

        dbContext.get("getCategorias", function (error, data) {
            return res.json(response(data, error));
        });
    }


    function getMaterial(req, res) {
        return res.json(req.data);
    }


    return {
        get: getMaterial,
        getAll: getMateriais,
        getC: getCategorias
    }
}

module.exports = MiscRepository;