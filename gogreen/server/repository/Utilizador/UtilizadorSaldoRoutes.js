const _utilizadorSaldoRepository = require('./UtilizadorSaldoRepository');
const dbContext = require('../../Database/dbContext');

module.exports = function (router) {
    const utilizadorSaldoRepository = _utilizadorSaldoRepository(dbContext);

    router.route('/utilizadorSaldo')
        .post(utilizadorSaldoRepository.post);

    router.use('/utilizadorSaldo/:id', utilizadorSaldoRepository.intercept);

    router.route('/utilizadorSaldo/:id')
        .get(utilizadorSaldoRepository.get)
}