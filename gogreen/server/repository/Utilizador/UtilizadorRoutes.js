const _utilizadorRepository = require('./UtilizadorRepository');
const dbContext = require('../../Database/dbContext');

module.exports = function (router) {
    const utilizadorRepository = _utilizadorRepository(dbContext);

    router.route('/utilizadores')
        .get(utilizadorRepository.getAll)
        .post(utilizadorRepository.post);

    router.use('/utilizadores/:utilizadorId', utilizadorRepository.intercept);

    router.route('/utilizadores/:utilizadorId')
        .get(utilizadorRepository.auth, utilizadorRepository.get)
        .put(utilizadorRepository.put)
        .delete(utilizadorRepository.delete);
}