const _utilizadorNotificacoesRepository = require('./UtilizadorNotificacoesRepository');
const dbContext = require('../../Database/dbContext');

module.exports = function (router) {
    const utilizadorNotificacoesRepository = _utilizadorNotificacoesRepository(dbContext);

       router.route('/utilizadorNotificacoes')
           .post(utilizadorNotificacoesRepository.post);

    router.use('/utilizadorNotificacoes/:id', utilizadorNotificacoesRepository.intercept);

    router.route('/utilizadorNotificacoes/:id')
        .get(utilizadorNotificacoesRepository.get)
        .delete(utilizadorNotificacoesRepository.delete);
}