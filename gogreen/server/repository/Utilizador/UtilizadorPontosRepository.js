var response = require('../../shared/response');
var TYPES = require('tedious').TYPES;

function UtilizadorPontosRepository(dbContext) {


    function getAllUtilizadorPoints(req, res, next) {

        if (req.params.id) {
            var parameters = [];

            parameters.push({ name: 'id_utilizador', type: TYPES.Int, val: req.params.id });

            var query = "select * from pontos"

            dbContext.getQuery(query, parameters, false, function (error, data) {
                if (data) {
                    req.data = data[0];
                    return next();
                }
                return res.sendStatus(404);
            });
        }
    }

     function getUtilizadorPontos(req, res, next) {

        if (req.params.Pontosid) {
            var parameters = [];

            
            parameters.push({ name: 'id_utilizador', type: TYPES.Int, val: req.params.Pontosid });

            var query = "select * from pontos where id_utilizador = " + req.params.Pontosid

            dbContext.getQuery(query, parameters, true, function (error, data) {
                if (!data) {
                    return res.status(404).json({erro: error});
                    console.log(error);
                }
                req.data = data[0];
                return next();
            });
        }
    }

    function getPontos(req, res) {
        return res.json(req.data);
    }

    function postUtilizadorPontos(req, res) {

        var parameters = [];


        parameters.push({ name: 'id_utilizador', type: TYPES.Int, val: req.body.id_utilizador });
        parameters.push({ name: 'data', type: TYPES.Date, val: req.body.data });
        parameters.push({ name: 'numPontos', type: TYPES.Int, val: req.body.numPontos });

        dbContext.post("InserirPontos", parameters, function (error, data) {
            return res.json(response(data, error));
        });
    }

    function putUtilizador(req, res) {

        var parameters = [];

        Object.entries(req.data).forEach((property) => {

            if (req.body[property[0]]) {
                if (property[0] === 'dataInscricao' || property[0] === 'ult_recolha') {
                    parameters.push(
                        {
                            name: property[0],
                            val: req.body[property[0]],
                            type: TYPES.Date
                        });
                } else {
                    parameters.push(
                        {
                            name: property[0],
                            val: req.body[property[0]],
                            type: TYPES.VarChar
                        });
                }
            } else {
                if (property[0] === 'dataInscricao' || property[0] === 'ult_recolha') {

                    parameters.push(
                        {
                            name: property[0],
                            val: property[1],
                            type: TYPES.Date
                        });
                } else {
                    parameters.push(
                        {
                            name: property[0],
                            val: property[1],
                            type: TYPES.VarChar
                        });
                }
            }
        });

        dbContext.post("InserirAlterarUtilizador", parameters, function (error, data) {
            return res.json(response(data, error));
        });
    }


    /**function SearchUtilizador(req, res) {

        var parameters = [];

        parameters.push({ name: 'Salary', type: TYPES.Int, val: req.query.salary });

        var query = "select * from tbl_employee where salary>=@Salary"

        dbContext.get(query, parameters, function (error, data) {
            return res.json(response(data, error));
        });
    }*/

    return {
        getAll: getAllUtilizadorPoints,
        get: getPontos,
        post: postUtilizadorPontos,
        put: putUtilizador,
        // find: SearchUtilizador,
        intercept: getUtilizadorPontos,
    }
}

module.exports = UtilizadorPontosRepository;