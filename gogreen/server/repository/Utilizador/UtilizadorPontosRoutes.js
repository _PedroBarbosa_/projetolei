const _utilizadorPontosRepository = require('./UtilizadorPontosRepository');
const dbContext = require('../../Database/dbContext');

module.exports = function (router) {
    const utilizadorPontosRepository = _utilizadorPontosRepository(dbContext);

       router.route('/utilizadorPontos')
           .get(utilizadorPontosRepository.getAll)
           .post(utilizadorPontosRepository.post);

    router.use('/utilizadorPontos/:Pontosid', utilizadorPontosRepository.intercept);

    router.route('/utilizadorPontos/:Pontosid')
        .get(utilizadorPontosRepository.get)
}