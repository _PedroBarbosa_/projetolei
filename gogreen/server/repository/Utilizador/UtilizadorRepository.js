var response = require('../../shared/response');
var TYPES = require('tedious').TYPES;
var bcrypt = require('bcrypt');
var jwt = require('jsonwebtoken');
require('dotenv').config();

function UtilizadorRepository(dbContext) {

    function findUtilizador(req, res, next) {

        if (req.params.utilizadorId) {
            var parameters = [];

            parameters.push({ name: 'id_utilizador', type: TYPES.Int, val: req.params.utilizadorId });

            var query = "select * from Utilizador where id_utilizador = " + req.params.utilizadorId

            dbContext.getQuery(query, parameters, false, function (error, data) {
                if (data) {
                    req.data = data[0];
                    return next();
                }
                return res.sendStatus(404);
            });
        }
    }

    function getUtilizadores(req, res) {

        dbContext.get("getUtilizadores", function (error, data) {
            return res.json(response(data, error));
        });
    }

    function getUtilizador(req, res) {
        return res.json(req.data);
    }

    function postUtilizador(req, res) {

        const hash = bcrypt.hashSync(req.body.password, 10);

        var parameters = [];

        parameters.push({ name: 'nome', type: TYPES.VarChar, val: req.body.nome });
        parameters.push({ name: 'email', type: TYPES.VarChar, val: req.body.email });
        parameters.push({ name: 'dataInscricao', type: TYPES.Date, val: req.body.dataInscricao });
        parameters.push({ name: 'morada', type: TYPES.VarChar, val: req.body.morada });
        parameters.push({ name: 'password', type: TYPES.VarChar, val: hash });
        parameters.push({ name: 'distrito', type: TYPES.VarChar, val: req.body.distrito });
        parameters.push({ name: 'ult_recolha', type: TYPES.Date, val: req.body.ult_recolha });
        parameters.push({ name: 'perfil_ativo_recolhas', type: TYPES.Bit, val: 0 });

        dbContext.post("InserirAlterarUtilizador", parameters, function (error, data) {
            return res.json(response(data, error));
        });
    }

    function putUtilizador(req, res) {

        var parameters = [];

        Object.entries(req.data).forEach((property) => {

            if (req.body[property[0]]) {
                if (property[0] === 'dataInscricao' || property[0] === 'ult_recolha') {
                    parameters.push(
                        {
                            name: property[0],
                            val: req.body[property[0]],
                            type: TYPES.Date
                        });
                } else {
                    if (property[0] === 'password') {
                        const hash = bcrypt.hashSync(req.body[property[0]], 10);
                        console.log(hash);

                        parameters.push({
                            name: property[0],
                            val: hash,
                            type: TYPES.VarChar
                        })
                    } else {
                        parameters.push(
                            {
                                name: property[0],
                                val: req.body[property[0]],
                                type: TYPES.VarChar
                            });
                    }
                }
            } else {
                if (property[0] === 'dataInscricao' || property[0] === 'ult_recolha') {

                    parameters.push(
                        {
                            name: property[0],
                            val: property[1],
                            type: TYPES.Date
                        });
                } else {
                    parameters.push(
                        {
                            name: property[0],
                            val: property[1],
                            type: TYPES.VarChar
                        });
                }
            }
        });

        dbContext.post("InserirAlterarUtilizador", parameters, function (error, data) {
            console.log(error);
            return res.json(response(data, error));
        });
    }

    function deleteUtilizador(req, res) {

        var parameters = [];
        console.log("teste");
        if (req.params.utilizadorId) {
            var parameters = [];

            console.log(req.data.utilizadorId);
            parameters.push({ name: 'id_utilizador', type: TYPES.Int, val: req.params.utilizadorId });

            var query = "delete from Utilizador where id_utilizador = " + req.params.utilizadorId;

            dbContext.getQuery(query, parameters, false, function (error, data, rowCount) {
                if (rowCount > 0) {
                    return res.json('Record is deleted');
                }
                return res.sendStatus(404);
            });
        }
    }

    /**function SearchUtilizador(req, res) {

        var parameters = [];

        parameters.push({ name: 'Salary', type: TYPES.Int, val: req.query.salary });

        var query = "select * from tbl_employee where salary>=@Salary"

        dbContext.get(query, parameters, function (error, data) {
            return res.json(response(data, error));
        });
    }*/

    function authenticateToken(req, res, next) {
        const authHeader = req.headers['authorization']

        const token = authHeader && authHeader.split(' ')[1];

        if (!token) return res.sendStatus(401);

        jwt.verify(token, process.env.JWT_SECRET, (err, user) => {
            if (err) return res.sendStatus(403);

            req.user = user;
            next();
        })

    }

    return {
        getAll: getUtilizadores,
        get: getUtilizador,
        post: postUtilizador,
        put: putUtilizador,
        // find: SearchUtilizador,
        intercept: findUtilizador,
        delete: deleteUtilizador,
        auth: authenticateToken
    }
}

module.exports = UtilizadorRepository;