var response = require('../../shared/response');
var TYPES = require('tedious').TYPES;

function UtilizadorSaldoRepository(dbContext) {

    function getUtilizadorSaldo(req, res, next) {

        if (req.params.id) {
            var parameters = [];

            parameters.push({ name: 'id_utilizador', type: TYPES.Int, val: req.params.id });

            var query = "select * from saldo_utilizador where id_utilizador = " + req.params.id

            dbContext.getQuery(query, parameters, true, function (error, data) {
                if (data) {
                    req.data = data[0];
                    return next();
                }
                console.log(error);
                return res.sendStatus(404);
            });
        }
    }

    function getSaldo(req, res) {
        return res.json(req.data);
    }

    function postUtilizadorSaldo(req, res) {

        var parameters = [];

        parameters.push({ name: 'id_utilizador', type: TYPES.Int, val: req.body.id_utilizador });
        parameters.push({ name: 'valor', type: TYPES.Int, val: req.body.valor });

        dbContext.post("AdicionarSaldo", parameters, function (error, data) {
            return res.json(response(data, error));
        });
    }

    /**function SearchUtilizador(req, res) {

        var parameters = [];

        parameters.push({ name: 'Salary', type: TYPES.Int, val: req.query.salary });

        var query = "select * from tbl_employee where salary>=@Salary"

        dbContext.get(query, parameters, function (error, data) {
            return res.json(response(data, error));
        });
    }*/

    return {
        get: getSaldo,
        post: postUtilizadorSaldo,
        //put: putUtilizador,
        // find: SearchUtilizador,
        intercept: getUtilizadorSaldo,
    }
}

module.exports = UtilizadorSaldoRepository;