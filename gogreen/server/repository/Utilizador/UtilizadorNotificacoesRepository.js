var response = require('../../shared/response');
var TYPES = require('tedious').TYPES;

function UtilizadorNotificacoesRepository(dbContext) {

    function getUtilizadorNotificacoes(req, res, next) {

        if (req.params.id) {
            var parameters = [];

            parameters.push({ name: 'id_utilizador', type: TYPES.Int, val: req.params.id });

            var query = "select * from notificacoes where id_utilizador = " + req.params.id +
                " Order By data"

            dbContext.getQuery(query, parameters, true, function (error, data) {
                if (data) {
                    req.data = data[0];
                    return next();
                }
                return res.sendStatus(404);
            });
        }
    }

    function getNotificacao(req, res) {
        return res.json(req.data);
    }

    function postUtilizadorNotificacao(req, res) {

        var parameters = [];

        parameters.push({ name: 'id_utilizador', type: TYPES.Int, val: req.body.id_utilizador });
        parameters.push({ name: 'titulo', type: TYPES.VarChar, val: req.body.titulo });
        parameters.push({ name: 'descricao', type: TYPES.VarChar, val: req.body.descricao });
        parameters.push({ name: 'data', type: TYPES.Date, val: req.body.data });

        dbContext.post("AdicionarNotificacao", parameters, function (error, data) {
            return res.json(response(data, error));
        });
    }

    function deleteUtilizadorNotificacoes(req, res) {

        var parameters = [];
        if (req.params.id) {
            var parameters = [];

            parameters.push({ name: 'id_utilizador', type: TYPES.Int, val: req.params.id });

            var query = "delete from notificacoes where id_utilizador = " + req.params.id;

            dbContext.getQuery(query, parameters, false, function (error, data, rowCount) {
                if (rowCount > 0) {
                    return res.json('Record is deleted');
                }
                return res.sendStatus(404);
            });
        }
    }

    /**function SearchUtilizador(req, res) {

        var parameters = [];

        parameters.push({ name: 'Salary', type: TYPES.Int, val: req.query.salary });

        var query = "select * from tbl_employee where salary>=@Salary"

        dbContext.get(query, parameters, function (error, data) {
            return res.json(response(data, error));
        });
    }*/

    return {
        get: getNotificacao,
        post: postUtilizadorNotificacao,
        //put: putUtilizador,
        // find: SearchUtilizador,
        intercept: getUtilizadorNotificacoes,
        delete: deleteUtilizadorNotificacoes
    }
}

module.exports = UtilizadorNotificacoesRepository;