const _recolhasMat = require('./recolhasMatRepository');
const dbContext = require('../../Database/dbContext');

module.exports = function (router) {
    const recolhasMat = _recolhasMat(dbContext);

    //recolhas para os ids dos materiais das recolhas
    router.route('/recolhasMat')
        //.get(recolhasMat.getAll)
        .post(recolhasMat.post);

    router.use('/recolhasMat/:id', recolhasMat.intercept);

    router.route('/recolhasMat/:id')
        .get(recolhasMat.get)
        .put(recolhasMat.put)
        .delete(recolhasMat.delete);
}