const _recolhas = require('./RecolhasRepository');
const dbContext = require('../../Database/dbContext');

module.exports = function (router) {
    const recolhas = _recolhas(dbContext);

    //recolhas para os ids das recolhas
    router.route('/recolhas')
        //.get(recolhas.getAll)
        .post(recolhas.post);

    router.use('/recolhas/:id', recolhas.intercept);

    router.route('/recolhas/:id')
        .get(recolhas.get)
        .put(recolhas.put)
        .delete(recolhas.delete);

    router.use('/recolhasDone/:id', recolhas.done)

    //recolhas para os ids dos utlizadores
    router.use('/recolhasU/:id', recolhas.interceptU);

    router.route('/recolhasU/:id')
        .get(recolhas.getU)
    //  .put(recolhas.put)
    //  .delete(recolhas.delete);

    //recolhas para os ids dos funcionarios
    router.use('/recolhasF/:id', recolhas.interceptF);

    router.route('/recolhasF/:id')
        .get(recolhas.getF)
    //  .put(recolhas.put)
    //  .delete(recolhas.delete);
}