var response = require('../../shared/response');
var TYPES = require('tedious').TYPES;

function RecolhasRepository(dbContext) {

    function findRecolha(req, res, next) {

        if (req.params.id) {
            var parameters = [];

            parameters.push({ name: 'id_recolha', type: TYPES.Int, val: req.params.id });

            var query = "select * from recolha JOIN recolha_materiais " +
                "ON recolha.id_recolha = recolha_materiais.id_recolha " +
                "where recolha.id_recolha = " + req.params.id

            console.log(query);

            dbContext.getQuery(query, parameters, true, function (error, data) {
                if (data) {
                    req.data = data[0];
                    return next();
                }
                return res.sendStatus(404);
            });
        }
    }

    function getRecolhas(req, res) {

        dbContext.get("getRecolhas", function (error, data) {
            return res.json(response(data, error));
        });
    }

    function getRecolha(req, res) {
        return res.json(req.data);
    }

    function postRecolha(req, res) {

        var parameters = [];

        parameters.push({ name: 'id_utilizador', type: TYPES.VarChar, val: req.body.id_utilizador });
        parameters.push({ name: 'id_funcionario', type: TYPES.VarChar, val: req.body.id_funcionario });
        parameters.push({ name: 'data', type: TYPES.Date, val: req.body.data });

        dbContext.post("InserirAlterarRecolha", parameters, function (error, data) {
            return res.json(response(data, error));
        });
    }

    function putRecolha(req, res) {

        var parameters = [];
        Object.entries(req.data).forEach((property) => {
            if (req.body[property[0]]) {
                parameters.push(
                    {
                        name: property[0],
                        val: req.body[property[0]],
                        type: TYPES.VarChar
                    });
            } else {
                parameters.push(
                    {
                        name: property[0],
                        val: property[1],
                        type: TYPES.VarChar
                    });
            }
        });

        dbContext.post("InserirAlterarRecolha", parameters, function (error, data) {
            return res.json(response(data, error));
        });
    }

    function doneRecolha(req, res) {

        if (req.params.id) {
            var parameters = []
            query = "UPDATE Recolha SET realizada = 1 WHERE id_recolha = " + req.params.id;

            dbContext.exeQuery(query, parameters, false, (error, data) => {
                if (!error) {
                    return res.sendStatus(200)
                }
                console.log(error)
                return res.status(400)
            })
        }
    }

    function deleteRecolha(req, res) {

        var parameters = [];

        if (req.params.id) {
            var parameters = [];

            parameters.push({ name: 'id_recolha', type: TYPES.Int, val: req.params.id });

            var query = "delete from recolha where id_recolha = " + req.params.id;

            dbContext.getQuery(query, parameters, false, function (error, data, rowCount) {
                if (rowCount > 0) {
                    return res.json('Record is deleted');
                }
                return res.sendStatus(404);
            });
        }
    }

    /**function SearchUtilizador(req, res) {

        var parameters = [];

        parameters.push({ name: 'Salary', type: TYPES.Int, val: req.query.salary });

        var query = "select * from tbl_employee where salary>=@Salary"

        dbContext.get(query, parameters, function (error, data) {
            return res.json(response(data, error));
        });
    }*/

    function findRecolhasByUserU(req, res, next) {

        if (req.params.id) {
            var parameters = [];

            parameters.push({ name: 'id_utilizador', type: TYPES.Int, val: req.params.id });

            var query = "select * from Recolha " +
                "JOIN recolha_materiais as rm " +
                "ON Recolha.id_recolha = rm.id_recolha " +
                "where Recolha.id_utilizador = " + req.params.id + " ORDER BY Recolha.data DESC"

            dbContext.getQuery(query, parameters, true, function (error, data) {
                if (data) {
                    req.data = data[0];
                    return next();
                }
                return res.sendStatus(404);
            });
        }
    }

    function getRecolhaU(req, res) {
        return res.json(req.data);
    }

    function findRecolhasByFuncF(req, res, next) {

        if (req.params.id) {
            var parameters = [];

            parameters.push({ name: 'id_funcionario', type: TYPES.Int, val: req.params.id });

            var query = "select * from recolha where id_funcionario = " + req.params.id

            dbContext.getQuery(query, parameters, true, function (error, data) {
                if (data) {
                    req.data = data[0];
                    return next();
                }
                return res.sendStatus(404);
            });
        }
    }

    function getRecolhaF(req, res) {
        return res.json(req.data);
    }

    return {
        //getAll: getRecolhas,
        get: getRecolha,
        post: postRecolha,
        put: putRecolha,
        // find: SearchUtilizador,
        intercept: findRecolha,
        delete: deleteRecolha,
        // find: findRecolha
        done: doneRecolha,
        interceptU: findRecolhasByUserU,
        getU: getRecolhaU,

        interceptF: findRecolhasByFuncF,
        getF: getRecolhaF
    }
}

module.exports = RecolhasRepository;