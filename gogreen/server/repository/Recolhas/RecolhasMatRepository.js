var response = require('../../shared/response');
var TYPES = require('tedious').TYPES;

function RecolhasMatRepository(dbContext) {

    function findRecolhaMat(req, res, next) {

        if (req.params.id) {
            var parameters = [];

            parameters.push({ name: 'id_recolha', type: TYPES.Int, val: req.params.id });

            var query = "select * from recolha_materiais where id_recolha = " + req.params.id

            dbContext.getQuery(query, parameters, true, function (error, data) {
                if (data) {
                    req.data = data[0];
                    return next();
                }
                return res.sendStatus(404);
            });
        }
    }

    function getRecolhaMat(req, res) {
        return res.json(req.data);
    }

    function postRecolhaMat(req, res) {

        var parameters = [];
        
        parameters.push({ name: 'id_recolha', type: TYPES.Int, val: req.body.id_recolha });
        parameters.push({ name: 'line', type: TYPES.Int, val: req.body.line });
        parameters.push({ name: 'quantidade', type: TYPES.Int, val: req.body.quantidade });
        parameters.push({ name: 'Nome_residuo', type: TYPES.VarChar, val: req.body.Nome_residuo });
        parameters.push({ name: 'cat', type: TYPES.VarChar, val: req.body.cat });

        dbContext.post("InsertMaterialRecolha", parameters, function (error, data) {
            return res.json(response(data, error));
        });
    }

    function putRecolhaMat(req, res) {

        var parameters = [];

        Object.entries(req.data).forEach((property) => {

            if (req.body[property[0]]) {
                parameters.push(
                    {
                        name: property[0],
                        val: req.body[property[0]],
                        type: TYPES.VarChar
                    });

            } else {
                parameters.push(
                    {
                        name: property[0],
                        val: property[1],
                        type: TYPES.VarChar
                    });
            }
        });

        dbContext.post("InserirAlterarRecolhaMat", parameters, function (error, data) {
            return res.json(response(data, error));
        });
    }

    function deleteRecolhaMat(req, res) {

        var parameters = [];

        if (req.params.id) {
            var parameters = [];

            parameters.push({ name: 'id_recolha', type: TYPES.Int, val: req.params.id });

            var query = "delete from recolha where id_recolha = " + req.params.id
            + " AND line = " + req.query.line;

            dbContext.getQuery(query, parameters, false, function (error, data, rowCount) {
                if (rowCount > 0) {
                    return res.json('Record is deleted');
                }
                return res.sendStatus(404);
            });
        }
    }

    return {
        //getAll: getRecolhas,
        get: getRecolhaMat,
        post: postRecolhaMat,
        put: putRecolhaMat,
        // find: SearchUtilizador,
        intercept: findRecolhaMat,
        delete: deleteRecolhaMat
    }
}

module.exports = RecolhasMatRepository;