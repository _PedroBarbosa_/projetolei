var jwt = require('jsonwebtoken');

function generateToken(utilizador) {
    if (!utilizador)
        return null;

    var u = {
        id_utilizador: utilizador.id_utilizador,
        nome: utilizador.nome,
        distrito: utilizador.distrito,
        morada: utilizador.morada,
    };

    return jwt.sign(u, process.env.JWT_SECRET, {
        expiresIn: 60 * 60 * 2 // 1h expira
    });
}

function getCleanUser(utilizador) {

    if (!utilizador)
        return null;

    return {
        id_utilizador: utilizador.id_utilizador,
        nome: utilizador.nome,
        distrito: utilizador.distrito
    };
}

function getCleanFunc(funcionario) {

    if (!funcionario)
        return null;

    return {
        id_funcionario: funcionario.id_funcionario,
        username: funcionario.username
    };
}

module.exports = {
    generateToken,
    getCleanUser,
    getCleanFunc
}