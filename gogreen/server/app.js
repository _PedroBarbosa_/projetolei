var express = require('express');
var bodyParser = require('body-parser');
require('dotenv').config();
const fileUpload = require('express-fileupload');

var app = express();

var port = process.env.port || 3300

app.listen(port, () => {
    console.log("Servidor ligado na porta: " + port);
});

var cors = require('cors');

app.use(cors());

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(fileUpload())

var router = require('./routes')();

app.use('/api', router);
