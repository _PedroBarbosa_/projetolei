const express = require('express');
const { Router } = require('express');

function eRoutes() {
    const router = express.Router();
    var utilizador = require('./repository/Utilizador/UtilizadorRoutes')(router);
    var funcionario = require('./repository/Funcionario/FuncionarioRoutes')(router);
    var utilizadorPontos = require('./repository/Utilizador/UtilizadorPontosRoutes')(router);
    var utilizadorSaldo = require('./repository/Utilizador/UtilizadorSaldoRoutes')(router)
    var utilizadorNotificacoes = require('./repository/Utilizador/UtilizadorNotificacoesRoutes')(router)
    var Campanha = require('./repository/Campanha/CampanhaRoutes')(router);
    var CampanhaUtilizador = require('./repository/Campanha/CampanhaUtilizadorRoutes')(router);
    var ProdutoReciclado = require('./repository/Produto_reciclado/ProdutoRecicladoRoutes')(router);
    var recolhas = require('./repository/Recolhas/RecolhasRoute')(router);
    var recolhasMat = require('./repository/Recolhas/RecolhasMatRoutes')(router);
    var login = require('./repository/Login/LoginRoutes')(router);
    var misc = require('./repository/Misc/MiscRoutes')(router);
    return router;
}

module.exports = eRoutes;