var Connection = require('tedious').Connection;

var config = {
    server: 'localhost',
    authentication: {
        type: 'default',
        options: {
            userName: 'Runo',
            password: 'qwerty'
        }
    },
    options: {
        database: 'GoGreen',
        instanceName: 'SQLSERVER2019',
        rowCollectionOnDone: true,
        useColumnNames: false
    }
}

var connection = new Connection(config);

connection.on('connect', function (err) {
    if (err) {
        console.log(err);
    } else {
        console.log('Connected at : '+ new Date());
    }
});

module.exports = connection;

