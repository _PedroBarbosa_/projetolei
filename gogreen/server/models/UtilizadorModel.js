var TYPES = require('tedious').TYPES;

const Utilizador = {
    nome: TYPES.VarChar,
    email: TYPES.VarChar,
    dataInscricao: TYPES.Date,
    morada: TYPES.VarChar,
    password: TYPES.VarChar,
    distrito: TYPES.VarChar,
    ult_recolha: TYPES.Date,
    perfil_ativo_recolha: TYPES.Bit
}
