var TYPES = require('tedious').TYPES;

const Funcionario = {
    nome: TYPES.VarChar,
    dataContrato: TYPES.Date,
    salario: TYPES.Float,
    deptNo: TYPES.Int,
    recolhasAtivas: TYPES.Int
}
